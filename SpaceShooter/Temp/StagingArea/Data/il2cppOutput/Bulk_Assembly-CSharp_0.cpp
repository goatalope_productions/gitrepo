﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// Boundary
struct Boundary_t1794889402;
// DestroyByBoundary
struct DestroyByBoundary_t3535450619;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Object
struct Object_t1021602117;
// DestroyByContact
struct DestroyByContact_t3397533383;
// System.String
struct String_t;
// GameController
struct GameController_t3607102586;
// UnityEngine.Transform
struct Transform_t3275118058;
// DestroyByTime
struct DestroyByTime_t1319923286;
// Done_BGScroller
struct Done_BGScroller_t1955972008;
// Done_Boundary
struct Done_Boundary_t381809497;
// Done_DestroyByBoundary
struct Done_DestroyByBoundary_t1508105942;
// Done_DestroyByContact
struct Done_DestroyByContact_t866731992;
// Done_GameController
struct Done_GameController_t2490533223;
// Done_DestroyByTime
struct Done_DestroyByTime_t1767105437;
// Done_EvasiveManeuver
struct Done_EvasiveManeuver_t2572524317;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// Done_EvasiveManeuver/<Evade>c__Iterator0
struct U3CEvadeU3Ec__Iterator0_t1651935502;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t3839502067;
// System.NotSupportedException
struct NotSupportedException_t1793819818;
// UnityEngine.GUIText
struct GUIText_t2411476300;
// Done_GameController/<SpawnWaves>c__Iterator0
struct U3CSpawnWavesU3Ec__Iterator0_t3736144616;
// Done_Mover
struct Done_Mover_t3207174306;
// Done_PlayerController
struct Done_PlayerController_t1238573128;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// Done_RandomRotator
struct Done_RandomRotator_t4185270575;
// Done_WeaponController
struct Done_WeaponController_t1887556875;
// GameController/<SpawnWaves>c__Iterator0
struct U3CSpawnWavesU3Ec__Iterator0_t495518909;
// Mover
struct Mover_t873752897;
// PlayerController
struct PlayerController_t4148409433;
// RandomRotator
struct RandomRotator_t1977862972;
// Readme
struct Readme_t795099402;
// UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470;
// Readme/Section
struct Section_t1120322242;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Void
struct Void_t1841601450;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// Readme/Section[]
struct SectionU5BU5D_t1354457079;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;

extern RuntimeClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const uint32_t DestroyByBoundary_OnTriggerExit_m4111137786_MetadataUsageId;
extern RuntimeClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_GetComponent_TisGameController_t3607102586_m57617221_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral40213808;
extern Il2CppCodeGenString* _stringLiteral2803466960;
extern const uint32_t DestroyByContact_Start_m888149736_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Object_Instantiate_TisGameObject_t1756533147_m3064851704_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2522967920;
extern Il2CppCodeGenString* _stringLiteral1875862075;
extern const uint32_t DestroyByContact_OnTriggerEnter_m3552195568_MetadataUsageId;
extern const uint32_t DestroyByTime_Start_m3534349595_MetadataUsageId;
extern RuntimeClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t Done_BGScroller_Update_m3812235626_MetadataUsageId;
extern const uint32_t Done_DestroyByBoundary_OnTriggerExit_m1927140173_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisDone_GameController_t2490533223_m3515554710_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral250741129;
extern const uint32_t Done_DestroyByContact_Start_m3161031753_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1816890106;
extern const uint32_t Done_DestroyByContact_OnTriggerEnter_m1153481469_MetadataUsageId;
extern const uint32_t Done_DestroyByTime_Start_m2732963720_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisRigidbody_t4233889191_m520013213_RuntimeMethod_var;
extern const uint32_t Done_EvasiveManeuver_Start_m589529130_MetadataUsageId;
extern RuntimeClass* U3CEvadeU3Ec__Iterator0_t1651935502_il2cpp_TypeInfo_var;
extern const uint32_t Done_EvasiveManeuver_Evade_m1091605597_MetadataUsageId;
extern RuntimeClass* Quaternion_t4030073918_il2cpp_TypeInfo_var;
extern const uint32_t Done_EvasiveManeuver_FixedUpdate_m4113228203_MetadataUsageId;
extern RuntimeClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CEvadeU3Ec__Iterator0_MoveNext_m3561999753_MetadataUsageId;
extern RuntimeClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CEvadeU3Ec__Iterator0_Reset_m4140678854_MetadataUsageId;
extern const uint32_t Done_GameController_Start_m1927832046_MetadataUsageId;
extern RuntimeClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const uint32_t Done_GameController_Update_m859186471_MetadataUsageId;
extern RuntimeClass* U3CSpawnWavesU3Ec__Iterator0_t3736144616_il2cpp_TypeInfo_var;
extern const uint32_t Done_GameController_SpawnWaves_m3768076309_MetadataUsageId;
extern RuntimeClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1756683522;
extern const uint32_t Done_GameController_UpdateScore_m1617972303_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2574498909;
extern const uint32_t Done_GameController_GameOver_m3639611404_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2187112517;
extern const uint32_t U3CSpawnWavesU3Ec__Iterator0_MoveNext_m1796056073_MetadataUsageId;
extern const uint32_t U3CSpawnWavesU3Ec__Iterator0_Reset_m4222166680_MetadataUsageId;
extern const uint32_t Done_Mover_Start_m1303039663_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisAudioSource_t1135106623_m3920278003_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral3645101709;
extern const uint32_t Done_PlayerController_Update_m1184947562_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral855845486;
extern Il2CppCodeGenString* _stringLiteral1635882288;
extern const uint32_t Done_PlayerController_FixedUpdate_m2702744912_MetadataUsageId;
extern const uint32_t Done_RandomRotator_Start_m3495564846_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3457519710;
extern const uint32_t Done_WeaponController_Start_m2637236048_MetadataUsageId;
extern const uint32_t Done_WeaponController_Fire_m86164314_MetadataUsageId;
extern const uint32_t GameController_Start_m239487205_MetadataUsageId;
extern const uint32_t GameController_Update_m1556003900_MetadataUsageId;
extern RuntimeClass* U3CSpawnWavesU3Ec__Iterator0_t495518909_il2cpp_TypeInfo_var;
extern const uint32_t GameController_SpawnWaves_m1643767934_MetadataUsageId;
extern const uint32_t GameController_UpdateScore_m1539112084_MetadataUsageId;
extern const uint32_t GameController_GameOver_m677971461_MetadataUsageId;
extern const uint32_t U3CSpawnWavesU3Ec__Iterator0_MoveNext_m40044830_MetadataUsageId;
extern const uint32_t U3CSpawnWavesU3Ec__Iterator0_Reset_m388647489_MetadataUsageId;
extern const uint32_t Mover_Start_m3361638920_MetadataUsageId;
extern const uint32_t PlayerController_Start_m3606284888_MetadataUsageId;
extern const uint32_t PlayerController_Update_m4228472513_MetadataUsageId;
extern const uint32_t PlayerController_FixedUpdate_m1756102567_MetadataUsageId;
extern const uint32_t RandomRotator_Start_m1184332417_MetadataUsageId;

struct GameObjectU5BU5D_t3057952154;


#ifndef U3CMODULEU3E_T3783534219_H
#define U3CMODULEU3E_T3783534219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534219 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534219_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef EXCEPTION_T1927440687_H
#define EXCEPTION_T1927440687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1927440687  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t169632028* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t1927440687 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t169632028* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t169632028** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t169632028* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___inner_exception_1)); }
	inline Exception_t1927440687 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t1927440687 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t1927440687 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T1927440687_H
#ifndef U3CEVADEU3EC__ITERATOR0_T1651935502_H
#define U3CEVADEU3EC__ITERATOR0_T1651935502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Done_EvasiveManeuver/<Evade>c__Iterator0
struct  U3CEvadeU3Ec__Iterator0_t1651935502  : public RuntimeObject
{
public:
	// Done_EvasiveManeuver Done_EvasiveManeuver/<Evade>c__Iterator0::$this
	Done_EvasiveManeuver_t2572524317 * ___U24this_0;
	// System.Object Done_EvasiveManeuver/<Evade>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Done_EvasiveManeuver/<Evade>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Done_EvasiveManeuver/<Evade>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CEvadeU3Ec__Iterator0_t1651935502, ___U24this_0)); }
	inline Done_EvasiveManeuver_t2572524317 * get_U24this_0() const { return ___U24this_0; }
	inline Done_EvasiveManeuver_t2572524317 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Done_EvasiveManeuver_t2572524317 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CEvadeU3Ec__Iterator0_t1651935502, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CEvadeU3Ec__Iterator0_t1651935502, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CEvadeU3Ec__Iterator0_t1651935502, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEVADEU3EC__ITERATOR0_T1651935502_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t1328083999* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t1328083999* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t1328083999** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t1328083999* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef DONE_BOUNDARY_T381809497_H
#define DONE_BOUNDARY_T381809497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Done_Boundary
struct  Done_Boundary_t381809497  : public RuntimeObject
{
public:
	// System.Single Done_Boundary::xMin
	float ___xMin_0;
	// System.Single Done_Boundary::xMax
	float ___xMax_1;
	// System.Single Done_Boundary::zMin
	float ___zMin_2;
	// System.Single Done_Boundary::zMax
	float ___zMax_3;

public:
	inline static int32_t get_offset_of_xMin_0() { return static_cast<int32_t>(offsetof(Done_Boundary_t381809497, ___xMin_0)); }
	inline float get_xMin_0() const { return ___xMin_0; }
	inline float* get_address_of_xMin_0() { return &___xMin_0; }
	inline void set_xMin_0(float value)
	{
		___xMin_0 = value;
	}

	inline static int32_t get_offset_of_xMax_1() { return static_cast<int32_t>(offsetof(Done_Boundary_t381809497, ___xMax_1)); }
	inline float get_xMax_1() const { return ___xMax_1; }
	inline float* get_address_of_xMax_1() { return &___xMax_1; }
	inline void set_xMax_1(float value)
	{
		___xMax_1 = value;
	}

	inline static int32_t get_offset_of_zMin_2() { return static_cast<int32_t>(offsetof(Done_Boundary_t381809497, ___zMin_2)); }
	inline float get_zMin_2() const { return ___zMin_2; }
	inline float* get_address_of_zMin_2() { return &___zMin_2; }
	inline void set_zMin_2(float value)
	{
		___zMin_2 = value;
	}

	inline static int32_t get_offset_of_zMax_3() { return static_cast<int32_t>(offsetof(Done_Boundary_t381809497, ___zMax_3)); }
	inline float get_zMax_3() const { return ___zMax_3; }
	inline float* get_address_of_zMax_3() { return &___zMax_3; }
	inline void set_zMax_3(float value)
	{
		___zMax_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONE_BOUNDARY_T381809497_H
#ifndef SECTION_T1120322242_H
#define SECTION_T1120322242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Readme/Section
struct  Section_t1120322242  : public RuntimeObject
{
public:
	// System.String Readme/Section::heading
	String_t* ___heading_0;
	// System.String Readme/Section::text
	String_t* ___text_1;
	// System.String Readme/Section::linkText
	String_t* ___linkText_2;
	// System.String Readme/Section::url
	String_t* ___url_3;

public:
	inline static int32_t get_offset_of_heading_0() { return static_cast<int32_t>(offsetof(Section_t1120322242, ___heading_0)); }
	inline String_t* get_heading_0() const { return ___heading_0; }
	inline String_t** get_address_of_heading_0() { return &___heading_0; }
	inline void set_heading_0(String_t* value)
	{
		___heading_0 = value;
		Il2CppCodeGenWriteBarrier((&___heading_0), value);
	}

	inline static int32_t get_offset_of_text_1() { return static_cast<int32_t>(offsetof(Section_t1120322242, ___text_1)); }
	inline String_t* get_text_1() const { return ___text_1; }
	inline String_t** get_address_of_text_1() { return &___text_1; }
	inline void set_text_1(String_t* value)
	{
		___text_1 = value;
		Il2CppCodeGenWriteBarrier((&___text_1), value);
	}

	inline static int32_t get_offset_of_linkText_2() { return static_cast<int32_t>(offsetof(Section_t1120322242, ___linkText_2)); }
	inline String_t* get_linkText_2() const { return ___linkText_2; }
	inline String_t** get_address_of_linkText_2() { return &___linkText_2; }
	inline void set_linkText_2(String_t* value)
	{
		___linkText_2 = value;
		Il2CppCodeGenWriteBarrier((&___linkText_2), value);
	}

	inline static int32_t get_offset_of_url_3() { return static_cast<int32_t>(offsetof(Section_t1120322242, ___url_3)); }
	inline String_t* get_url_3() const { return ___url_3; }
	inline String_t** get_address_of_url_3() { return &___url_3; }
	inline void set_url_3(String_t* value)
	{
		___url_3 = value;
		Il2CppCodeGenWriteBarrier((&___url_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECTION_T1120322242_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef U3CSPAWNWAVESU3EC__ITERATOR0_T495518909_H
#define U3CSPAWNWAVESU3EC__ITERATOR0_T495518909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameController/<SpawnWaves>c__Iterator0
struct  U3CSpawnWavesU3Ec__Iterator0_t495518909  : public RuntimeObject
{
public:
	// GameController GameController/<SpawnWaves>c__Iterator0::$this
	GameController_t3607102586 * ___U24this_0;
	// System.Object GameController/<SpawnWaves>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean GameController/<SpawnWaves>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 GameController/<SpawnWaves>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CSpawnWavesU3Ec__Iterator0_t495518909, ___U24this_0)); }
	inline GameController_t3607102586 * get_U24this_0() const { return ___U24this_0; }
	inline GameController_t3607102586 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(GameController_t3607102586 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CSpawnWavesU3Ec__Iterator0_t495518909, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CSpawnWavesU3Ec__Iterator0_t495518909, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CSpawnWavesU3Ec__Iterator0_t495518909, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSPAWNWAVESU3EC__ITERATOR0_T495518909_H
#ifndef YIELDINSTRUCTION_T3462875981_H
#define YIELDINSTRUCTION_T3462875981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t3462875981  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t3462875981_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t3462875981_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T3462875981_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef BOUNDARY_T1794889402_H
#define BOUNDARY_T1794889402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boundary
struct  Boundary_t1794889402  : public RuntimeObject
{
public:
	// System.Single Boundary::xMin
	float ___xMin_0;
	// System.Single Boundary::xMax
	float ___xMax_1;
	// System.Single Boundary::zMin
	float ___zMin_2;
	// System.Single Boundary::zMax
	float ___zMax_3;

public:
	inline static int32_t get_offset_of_xMin_0() { return static_cast<int32_t>(offsetof(Boundary_t1794889402, ___xMin_0)); }
	inline float get_xMin_0() const { return ___xMin_0; }
	inline float* get_address_of_xMin_0() { return &___xMin_0; }
	inline void set_xMin_0(float value)
	{
		___xMin_0 = value;
	}

	inline static int32_t get_offset_of_xMax_1() { return static_cast<int32_t>(offsetof(Boundary_t1794889402, ___xMax_1)); }
	inline float get_xMax_1() const { return ___xMax_1; }
	inline float* get_address_of_xMax_1() { return &___xMax_1; }
	inline void set_xMax_1(float value)
	{
		___xMax_1 = value;
	}

	inline static int32_t get_offset_of_zMin_2() { return static_cast<int32_t>(offsetof(Boundary_t1794889402, ___zMin_2)); }
	inline float get_zMin_2() const { return ___zMin_2; }
	inline float* get_address_of_zMin_2() { return &___zMin_2; }
	inline void set_zMin_2(float value)
	{
		___zMin_2 = value;
	}

	inline static int32_t get_offset_of_zMax_3() { return static_cast<int32_t>(offsetof(Boundary_t1794889402, ___zMax_3)); }
	inline float get_zMax_3() const { return ___zMax_3; }
	inline float* get_address_of_zMax_3() { return &___zMax_3; }
	inline void set_zMax_3(float value)
	{
		___zMax_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDARY_T1794889402_H
#ifndef SINGLE_T2076509932_H
#define SINGLE_T2076509932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t2076509932 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t2076509932, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T2076509932_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef UINT32_T2149682021_H
#define UINT32_T2149682021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2149682021 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t2149682021, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2149682021_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef WAITFORSECONDS_T3839502067_H
#define WAITFORSECONDS_T3839502067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitForSeconds
struct  WaitForSeconds_t3839502067  : public YieldInstruction_t3462875981
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t3839502067, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t3839502067_marshaled_pinvoke : public YieldInstruction_t3462875981_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t3839502067_marshaled_com : public YieldInstruction_t3462875981_marshaled_com
{
	float ___m_Seconds_0;
};
#endif // WAITFORSECONDS_T3839502067_H
#ifndef SCENE_T1684909666_H
#define SCENE_T1684909666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SceneManagement.Scene
struct  Scene_t1684909666 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t1684909666, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE_T1684909666_H
#ifndef INT32_T2071877448_H
#define INT32_T2071877448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2071877448 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2071877448, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2071877448_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef SYSTEMEXCEPTION_T3877406272_H
#define SYSTEMEXCEPTION_T3877406272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t3877406272  : public Exception_t1927440687
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T3877406272_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef QUATERNION_T4030073918_H
#define QUATERNION_T4030073918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t4030073918 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t4030073918_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t4030073918  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t4030073918  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t4030073918 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t4030073918  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T4030073918_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef KEYCODE_T2283395152_H
#define KEYCODE_T2283395152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2283395152 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t2283395152, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2283395152_H
#ifndef COROUTINE_T2299508840_H
#define COROUTINE_T2299508840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_t2299508840  : public YieldInstruction_t3462875981
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	IntPtr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t2299508840, ___m_Ptr_0)); }
	inline IntPtr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline IntPtr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(IntPtr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t2299508840_marshaled_pinvoke : public YieldInstruction_t3462875981_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t2299508840_marshaled_com : public YieldInstruction_t3462875981_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_T2299508840_H
#ifndef U3CSPAWNWAVESU3EC__ITERATOR0_T3736144616_H
#define U3CSPAWNWAVESU3EC__ITERATOR0_T3736144616_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Done_GameController/<SpawnWaves>c__Iterator0
struct  U3CSpawnWavesU3Ec__Iterator0_t3736144616  : public RuntimeObject
{
public:
	// System.Int32 Done_GameController/<SpawnWaves>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// UnityEngine.GameObject Done_GameController/<SpawnWaves>c__Iterator0::<hazard>__2
	GameObject_t1756533147 * ___U3ChazardU3E__2_1;
	// UnityEngine.Vector3 Done_GameController/<SpawnWaves>c__Iterator0::<spawnPosition>__2
	Vector3_t2243707580  ___U3CspawnPositionU3E__2_2;
	// UnityEngine.Quaternion Done_GameController/<SpawnWaves>c__Iterator0::<spawnRotation>__2
	Quaternion_t4030073918  ___U3CspawnRotationU3E__2_3;
	// Done_GameController Done_GameController/<SpawnWaves>c__Iterator0::$this
	Done_GameController_t2490533223 * ___U24this_4;
	// System.Object Done_GameController/<SpawnWaves>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean Done_GameController/<SpawnWaves>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 Done_GameController/<SpawnWaves>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CSpawnWavesU3Ec__Iterator0_t3736144616, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U3ChazardU3E__2_1() { return static_cast<int32_t>(offsetof(U3CSpawnWavesU3Ec__Iterator0_t3736144616, ___U3ChazardU3E__2_1)); }
	inline GameObject_t1756533147 * get_U3ChazardU3E__2_1() const { return ___U3ChazardU3E__2_1; }
	inline GameObject_t1756533147 ** get_address_of_U3ChazardU3E__2_1() { return &___U3ChazardU3E__2_1; }
	inline void set_U3ChazardU3E__2_1(GameObject_t1756533147 * value)
	{
		___U3ChazardU3E__2_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ChazardU3E__2_1), value);
	}

	inline static int32_t get_offset_of_U3CspawnPositionU3E__2_2() { return static_cast<int32_t>(offsetof(U3CSpawnWavesU3Ec__Iterator0_t3736144616, ___U3CspawnPositionU3E__2_2)); }
	inline Vector3_t2243707580  get_U3CspawnPositionU3E__2_2() const { return ___U3CspawnPositionU3E__2_2; }
	inline Vector3_t2243707580 * get_address_of_U3CspawnPositionU3E__2_2() { return &___U3CspawnPositionU3E__2_2; }
	inline void set_U3CspawnPositionU3E__2_2(Vector3_t2243707580  value)
	{
		___U3CspawnPositionU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CspawnRotationU3E__2_3() { return static_cast<int32_t>(offsetof(U3CSpawnWavesU3Ec__Iterator0_t3736144616, ___U3CspawnRotationU3E__2_3)); }
	inline Quaternion_t4030073918  get_U3CspawnRotationU3E__2_3() const { return ___U3CspawnRotationU3E__2_3; }
	inline Quaternion_t4030073918 * get_address_of_U3CspawnRotationU3E__2_3() { return &___U3CspawnRotationU3E__2_3; }
	inline void set_U3CspawnRotationU3E__2_3(Quaternion_t4030073918  value)
	{
		___U3CspawnRotationU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CSpawnWavesU3Ec__Iterator0_t3736144616, ___U24this_4)); }
	inline Done_GameController_t2490533223 * get_U24this_4() const { return ___U24this_4; }
	inline Done_GameController_t2490533223 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(Done_GameController_t2490533223 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CSpawnWavesU3Ec__Iterator0_t3736144616, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CSpawnWavesU3Ec__Iterator0_t3736144616, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CSpawnWavesU3Ec__Iterator0_t3736144616, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSPAWNWAVESU3EC__ITERATOR0_T3736144616_H
#ifndef NOTSUPPORTEDEXCEPTION_T1793819818_H
#define NOTSUPPORTEDEXCEPTION_T1793819818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1793819818  : public SystemException_t3877406272
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1793819818_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef SCRIPTABLEOBJECT_T1975622470_H
#define SCRIPTABLEOBJECT_T1975622470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1975622470  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_pinvoke : public Object_t1021602117_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_com : public Object_t1021602117_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1975622470_H
#ifndef GAMEOBJECT_T1756533147_H
#define GAMEOBJECT_T1756533147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1756533147  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1756533147_H
#ifndef RIGIDBODY_T4233889191_H
#define RIGIDBODY_T4233889191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody
struct  Rigidbody_t4233889191  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY_T4233889191_H
#ifndef TRANSFORM_T3275118058_H
#define TRANSFORM_T3275118058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3275118058  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3275118058_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef README_T795099402_H
#define README_T795099402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Readme
struct  Readme_t795099402  : public ScriptableObject_t1975622470
{
public:
	// UnityEngine.Texture2D Readme::icon
	Texture2D_t3542995729 * ___icon_2;
	// System.String Readme::title
	String_t* ___title_3;
	// Readme/Section[] Readme::sections
	SectionU5BU5D_t1354457079* ___sections_4;
	// System.Boolean Readme::loadedLayout
	bool ___loadedLayout_5;

public:
	inline static int32_t get_offset_of_icon_2() { return static_cast<int32_t>(offsetof(Readme_t795099402, ___icon_2)); }
	inline Texture2D_t3542995729 * get_icon_2() const { return ___icon_2; }
	inline Texture2D_t3542995729 ** get_address_of_icon_2() { return &___icon_2; }
	inline void set_icon_2(Texture2D_t3542995729 * value)
	{
		___icon_2 = value;
		Il2CppCodeGenWriteBarrier((&___icon_2), value);
	}

	inline static int32_t get_offset_of_title_3() { return static_cast<int32_t>(offsetof(Readme_t795099402, ___title_3)); }
	inline String_t* get_title_3() const { return ___title_3; }
	inline String_t** get_address_of_title_3() { return &___title_3; }
	inline void set_title_3(String_t* value)
	{
		___title_3 = value;
		Il2CppCodeGenWriteBarrier((&___title_3), value);
	}

	inline static int32_t get_offset_of_sections_4() { return static_cast<int32_t>(offsetof(Readme_t795099402, ___sections_4)); }
	inline SectionU5BU5D_t1354457079* get_sections_4() const { return ___sections_4; }
	inline SectionU5BU5D_t1354457079** get_address_of_sections_4() { return &___sections_4; }
	inline void set_sections_4(SectionU5BU5D_t1354457079* value)
	{
		___sections_4 = value;
		Il2CppCodeGenWriteBarrier((&___sections_4), value);
	}

	inline static int32_t get_offset_of_loadedLayout_5() { return static_cast<int32_t>(offsetof(Readme_t795099402, ___loadedLayout_5)); }
	inline bool get_loadedLayout_5() const { return ___loadedLayout_5; }
	inline bool* get_address_of_loadedLayout_5() { return &___loadedLayout_5; }
	inline void set_loadedLayout_5(bool value)
	{
		___loadedLayout_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // README_T795099402_H
#ifndef COLLIDER_T3497673348_H
#define COLLIDER_T3497673348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider
struct  Collider_t3497673348  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER_T3497673348_H
#ifndef AUDIOSOURCE_T1135106623_H
#define AUDIOSOURCE_T1135106623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioSource
struct  AudioSource_t1135106623  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOSOURCE_T1135106623_H
#ifndef GUIELEMENT_T3381083099_H
#define GUIELEMENT_T3381083099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIElement
struct  GUIElement_t3381083099  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIELEMENT_T3381083099_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef DONE_DESTROYBYTIME_T1767105437_H
#define DONE_DESTROYBYTIME_T1767105437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Done_DestroyByTime
struct  Done_DestroyByTime_t1767105437  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Done_DestroyByTime::lifetime
	float ___lifetime_2;

public:
	inline static int32_t get_offset_of_lifetime_2() { return static_cast<int32_t>(offsetof(Done_DestroyByTime_t1767105437, ___lifetime_2)); }
	inline float get_lifetime_2() const { return ___lifetime_2; }
	inline float* get_address_of_lifetime_2() { return &___lifetime_2; }
	inline void set_lifetime_2(float value)
	{
		___lifetime_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONE_DESTROYBYTIME_T1767105437_H
#ifndef DESTROYBYTIME_T1319923286_H
#define DESTROYBYTIME_T1319923286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DestroyByTime
struct  DestroyByTime_t1319923286  : public MonoBehaviour_t1158329972
{
public:
	// System.Single DestroyByTime::lifetime
	float ___lifetime_2;

public:
	inline static int32_t get_offset_of_lifetime_2() { return static_cast<int32_t>(offsetof(DestroyByTime_t1319923286, ___lifetime_2)); }
	inline float get_lifetime_2() const { return ___lifetime_2; }
	inline float* get_address_of_lifetime_2() { return &___lifetime_2; }
	inline void set_lifetime_2(float value)
	{
		___lifetime_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYBYTIME_T1319923286_H
#ifndef DONE_GAMECONTROLLER_T2490533223_H
#define DONE_GAMECONTROLLER_T2490533223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Done_GameController
struct  Done_GameController_t2490533223  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] Done_GameController::hazards
	GameObjectU5BU5D_t3057952154* ___hazards_2;
	// UnityEngine.Vector3 Done_GameController::spawnValues
	Vector3_t2243707580  ___spawnValues_3;
	// System.Int32 Done_GameController::hazardCount
	int32_t ___hazardCount_4;
	// System.Single Done_GameController::spawnWait
	float ___spawnWait_5;
	// System.Single Done_GameController::startWait
	float ___startWait_6;
	// System.Single Done_GameController::waveWait
	float ___waveWait_7;
	// UnityEngine.GUIText Done_GameController::scoreText
	GUIText_t2411476300 * ___scoreText_8;
	// UnityEngine.GUIText Done_GameController::restartText
	GUIText_t2411476300 * ___restartText_9;
	// UnityEngine.GUIText Done_GameController::gameOverText
	GUIText_t2411476300 * ___gameOverText_10;
	// System.Boolean Done_GameController::gameOver
	bool ___gameOver_11;
	// System.Boolean Done_GameController::restart
	bool ___restart_12;
	// System.Int32 Done_GameController::score
	int32_t ___score_13;

public:
	inline static int32_t get_offset_of_hazards_2() { return static_cast<int32_t>(offsetof(Done_GameController_t2490533223, ___hazards_2)); }
	inline GameObjectU5BU5D_t3057952154* get_hazards_2() const { return ___hazards_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_hazards_2() { return &___hazards_2; }
	inline void set_hazards_2(GameObjectU5BU5D_t3057952154* value)
	{
		___hazards_2 = value;
		Il2CppCodeGenWriteBarrier((&___hazards_2), value);
	}

	inline static int32_t get_offset_of_spawnValues_3() { return static_cast<int32_t>(offsetof(Done_GameController_t2490533223, ___spawnValues_3)); }
	inline Vector3_t2243707580  get_spawnValues_3() const { return ___spawnValues_3; }
	inline Vector3_t2243707580 * get_address_of_spawnValues_3() { return &___spawnValues_3; }
	inline void set_spawnValues_3(Vector3_t2243707580  value)
	{
		___spawnValues_3 = value;
	}

	inline static int32_t get_offset_of_hazardCount_4() { return static_cast<int32_t>(offsetof(Done_GameController_t2490533223, ___hazardCount_4)); }
	inline int32_t get_hazardCount_4() const { return ___hazardCount_4; }
	inline int32_t* get_address_of_hazardCount_4() { return &___hazardCount_4; }
	inline void set_hazardCount_4(int32_t value)
	{
		___hazardCount_4 = value;
	}

	inline static int32_t get_offset_of_spawnWait_5() { return static_cast<int32_t>(offsetof(Done_GameController_t2490533223, ___spawnWait_5)); }
	inline float get_spawnWait_5() const { return ___spawnWait_5; }
	inline float* get_address_of_spawnWait_5() { return &___spawnWait_5; }
	inline void set_spawnWait_5(float value)
	{
		___spawnWait_5 = value;
	}

	inline static int32_t get_offset_of_startWait_6() { return static_cast<int32_t>(offsetof(Done_GameController_t2490533223, ___startWait_6)); }
	inline float get_startWait_6() const { return ___startWait_6; }
	inline float* get_address_of_startWait_6() { return &___startWait_6; }
	inline void set_startWait_6(float value)
	{
		___startWait_6 = value;
	}

	inline static int32_t get_offset_of_waveWait_7() { return static_cast<int32_t>(offsetof(Done_GameController_t2490533223, ___waveWait_7)); }
	inline float get_waveWait_7() const { return ___waveWait_7; }
	inline float* get_address_of_waveWait_7() { return &___waveWait_7; }
	inline void set_waveWait_7(float value)
	{
		___waveWait_7 = value;
	}

	inline static int32_t get_offset_of_scoreText_8() { return static_cast<int32_t>(offsetof(Done_GameController_t2490533223, ___scoreText_8)); }
	inline GUIText_t2411476300 * get_scoreText_8() const { return ___scoreText_8; }
	inline GUIText_t2411476300 ** get_address_of_scoreText_8() { return &___scoreText_8; }
	inline void set_scoreText_8(GUIText_t2411476300 * value)
	{
		___scoreText_8 = value;
		Il2CppCodeGenWriteBarrier((&___scoreText_8), value);
	}

	inline static int32_t get_offset_of_restartText_9() { return static_cast<int32_t>(offsetof(Done_GameController_t2490533223, ___restartText_9)); }
	inline GUIText_t2411476300 * get_restartText_9() const { return ___restartText_9; }
	inline GUIText_t2411476300 ** get_address_of_restartText_9() { return &___restartText_9; }
	inline void set_restartText_9(GUIText_t2411476300 * value)
	{
		___restartText_9 = value;
		Il2CppCodeGenWriteBarrier((&___restartText_9), value);
	}

	inline static int32_t get_offset_of_gameOverText_10() { return static_cast<int32_t>(offsetof(Done_GameController_t2490533223, ___gameOverText_10)); }
	inline GUIText_t2411476300 * get_gameOverText_10() const { return ___gameOverText_10; }
	inline GUIText_t2411476300 ** get_address_of_gameOverText_10() { return &___gameOverText_10; }
	inline void set_gameOverText_10(GUIText_t2411476300 * value)
	{
		___gameOverText_10 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverText_10), value);
	}

	inline static int32_t get_offset_of_gameOver_11() { return static_cast<int32_t>(offsetof(Done_GameController_t2490533223, ___gameOver_11)); }
	inline bool get_gameOver_11() const { return ___gameOver_11; }
	inline bool* get_address_of_gameOver_11() { return &___gameOver_11; }
	inline void set_gameOver_11(bool value)
	{
		___gameOver_11 = value;
	}

	inline static int32_t get_offset_of_restart_12() { return static_cast<int32_t>(offsetof(Done_GameController_t2490533223, ___restart_12)); }
	inline bool get_restart_12() const { return ___restart_12; }
	inline bool* get_address_of_restart_12() { return &___restart_12; }
	inline void set_restart_12(bool value)
	{
		___restart_12 = value;
	}

	inline static int32_t get_offset_of_score_13() { return static_cast<int32_t>(offsetof(Done_GameController_t2490533223, ___score_13)); }
	inline int32_t get_score_13() const { return ___score_13; }
	inline int32_t* get_address_of_score_13() { return &___score_13; }
	inline void set_score_13(int32_t value)
	{
		___score_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONE_GAMECONTROLLER_T2490533223_H
#ifndef DONE_EVASIVEMANEUVER_T2572524317_H
#define DONE_EVASIVEMANEUVER_T2572524317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Done_EvasiveManeuver
struct  Done_EvasiveManeuver_t2572524317  : public MonoBehaviour_t1158329972
{
public:
	// Done_Boundary Done_EvasiveManeuver::boundary
	Done_Boundary_t381809497 * ___boundary_2;
	// System.Single Done_EvasiveManeuver::tilt
	float ___tilt_3;
	// System.Single Done_EvasiveManeuver::dodge
	float ___dodge_4;
	// System.Single Done_EvasiveManeuver::smoothing
	float ___smoothing_5;
	// UnityEngine.Vector2 Done_EvasiveManeuver::startWait
	Vector2_t2243707579  ___startWait_6;
	// UnityEngine.Vector2 Done_EvasiveManeuver::maneuverTime
	Vector2_t2243707579  ___maneuverTime_7;
	// UnityEngine.Vector2 Done_EvasiveManeuver::maneuverWait
	Vector2_t2243707579  ___maneuverWait_8;
	// System.Single Done_EvasiveManeuver::currentSpeed
	float ___currentSpeed_9;
	// System.Single Done_EvasiveManeuver::targetManeuver
	float ___targetManeuver_10;

public:
	inline static int32_t get_offset_of_boundary_2() { return static_cast<int32_t>(offsetof(Done_EvasiveManeuver_t2572524317, ___boundary_2)); }
	inline Done_Boundary_t381809497 * get_boundary_2() const { return ___boundary_2; }
	inline Done_Boundary_t381809497 ** get_address_of_boundary_2() { return &___boundary_2; }
	inline void set_boundary_2(Done_Boundary_t381809497 * value)
	{
		___boundary_2 = value;
		Il2CppCodeGenWriteBarrier((&___boundary_2), value);
	}

	inline static int32_t get_offset_of_tilt_3() { return static_cast<int32_t>(offsetof(Done_EvasiveManeuver_t2572524317, ___tilt_3)); }
	inline float get_tilt_3() const { return ___tilt_3; }
	inline float* get_address_of_tilt_3() { return &___tilt_3; }
	inline void set_tilt_3(float value)
	{
		___tilt_3 = value;
	}

	inline static int32_t get_offset_of_dodge_4() { return static_cast<int32_t>(offsetof(Done_EvasiveManeuver_t2572524317, ___dodge_4)); }
	inline float get_dodge_4() const { return ___dodge_4; }
	inline float* get_address_of_dodge_4() { return &___dodge_4; }
	inline void set_dodge_4(float value)
	{
		___dodge_4 = value;
	}

	inline static int32_t get_offset_of_smoothing_5() { return static_cast<int32_t>(offsetof(Done_EvasiveManeuver_t2572524317, ___smoothing_5)); }
	inline float get_smoothing_5() const { return ___smoothing_5; }
	inline float* get_address_of_smoothing_5() { return &___smoothing_5; }
	inline void set_smoothing_5(float value)
	{
		___smoothing_5 = value;
	}

	inline static int32_t get_offset_of_startWait_6() { return static_cast<int32_t>(offsetof(Done_EvasiveManeuver_t2572524317, ___startWait_6)); }
	inline Vector2_t2243707579  get_startWait_6() const { return ___startWait_6; }
	inline Vector2_t2243707579 * get_address_of_startWait_6() { return &___startWait_6; }
	inline void set_startWait_6(Vector2_t2243707579  value)
	{
		___startWait_6 = value;
	}

	inline static int32_t get_offset_of_maneuverTime_7() { return static_cast<int32_t>(offsetof(Done_EvasiveManeuver_t2572524317, ___maneuverTime_7)); }
	inline Vector2_t2243707579  get_maneuverTime_7() const { return ___maneuverTime_7; }
	inline Vector2_t2243707579 * get_address_of_maneuverTime_7() { return &___maneuverTime_7; }
	inline void set_maneuverTime_7(Vector2_t2243707579  value)
	{
		___maneuverTime_7 = value;
	}

	inline static int32_t get_offset_of_maneuverWait_8() { return static_cast<int32_t>(offsetof(Done_EvasiveManeuver_t2572524317, ___maneuverWait_8)); }
	inline Vector2_t2243707579  get_maneuverWait_8() const { return ___maneuverWait_8; }
	inline Vector2_t2243707579 * get_address_of_maneuverWait_8() { return &___maneuverWait_8; }
	inline void set_maneuverWait_8(Vector2_t2243707579  value)
	{
		___maneuverWait_8 = value;
	}

	inline static int32_t get_offset_of_currentSpeed_9() { return static_cast<int32_t>(offsetof(Done_EvasiveManeuver_t2572524317, ___currentSpeed_9)); }
	inline float get_currentSpeed_9() const { return ___currentSpeed_9; }
	inline float* get_address_of_currentSpeed_9() { return &___currentSpeed_9; }
	inline void set_currentSpeed_9(float value)
	{
		___currentSpeed_9 = value;
	}

	inline static int32_t get_offset_of_targetManeuver_10() { return static_cast<int32_t>(offsetof(Done_EvasiveManeuver_t2572524317, ___targetManeuver_10)); }
	inline float get_targetManeuver_10() const { return ___targetManeuver_10; }
	inline float* get_address_of_targetManeuver_10() { return &___targetManeuver_10; }
	inline void set_targetManeuver_10(float value)
	{
		___targetManeuver_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONE_EVASIVEMANEUVER_T2572524317_H
#ifndef DESTROYBYCONTACT_T3397533383_H
#define DESTROYBYCONTACT_T3397533383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DestroyByContact
struct  DestroyByContact_t3397533383  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject DestroyByContact::explosion
	GameObject_t1756533147 * ___explosion_2;
	// UnityEngine.GameObject DestroyByContact::playerExplosion
	GameObject_t1756533147 * ___playerExplosion_3;
	// GameController DestroyByContact::gameController
	GameController_t3607102586 * ___gameController_4;
	// System.Int32 DestroyByContact::scoreValue
	int32_t ___scoreValue_5;

public:
	inline static int32_t get_offset_of_explosion_2() { return static_cast<int32_t>(offsetof(DestroyByContact_t3397533383, ___explosion_2)); }
	inline GameObject_t1756533147 * get_explosion_2() const { return ___explosion_2; }
	inline GameObject_t1756533147 ** get_address_of_explosion_2() { return &___explosion_2; }
	inline void set_explosion_2(GameObject_t1756533147 * value)
	{
		___explosion_2 = value;
		Il2CppCodeGenWriteBarrier((&___explosion_2), value);
	}

	inline static int32_t get_offset_of_playerExplosion_3() { return static_cast<int32_t>(offsetof(DestroyByContact_t3397533383, ___playerExplosion_3)); }
	inline GameObject_t1756533147 * get_playerExplosion_3() const { return ___playerExplosion_3; }
	inline GameObject_t1756533147 ** get_address_of_playerExplosion_3() { return &___playerExplosion_3; }
	inline void set_playerExplosion_3(GameObject_t1756533147 * value)
	{
		___playerExplosion_3 = value;
		Il2CppCodeGenWriteBarrier((&___playerExplosion_3), value);
	}

	inline static int32_t get_offset_of_gameController_4() { return static_cast<int32_t>(offsetof(DestroyByContact_t3397533383, ___gameController_4)); }
	inline GameController_t3607102586 * get_gameController_4() const { return ___gameController_4; }
	inline GameController_t3607102586 ** get_address_of_gameController_4() { return &___gameController_4; }
	inline void set_gameController_4(GameController_t3607102586 * value)
	{
		___gameController_4 = value;
		Il2CppCodeGenWriteBarrier((&___gameController_4), value);
	}

	inline static int32_t get_offset_of_scoreValue_5() { return static_cast<int32_t>(offsetof(DestroyByContact_t3397533383, ___scoreValue_5)); }
	inline int32_t get_scoreValue_5() const { return ___scoreValue_5; }
	inline int32_t* get_address_of_scoreValue_5() { return &___scoreValue_5; }
	inline void set_scoreValue_5(int32_t value)
	{
		___scoreValue_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYBYCONTACT_T3397533383_H
#ifndef DONE_WEAPONCONTROLLER_T1887556875_H
#define DONE_WEAPONCONTROLLER_T1887556875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Done_WeaponController
struct  Done_WeaponController_t1887556875  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Done_WeaponController::shot
	GameObject_t1756533147 * ___shot_2;
	// UnityEngine.Transform Done_WeaponController::shotSpawn
	Transform_t3275118058 * ___shotSpawn_3;
	// System.Single Done_WeaponController::fireRate
	float ___fireRate_4;
	// System.Single Done_WeaponController::delay
	float ___delay_5;

public:
	inline static int32_t get_offset_of_shot_2() { return static_cast<int32_t>(offsetof(Done_WeaponController_t1887556875, ___shot_2)); }
	inline GameObject_t1756533147 * get_shot_2() const { return ___shot_2; }
	inline GameObject_t1756533147 ** get_address_of_shot_2() { return &___shot_2; }
	inline void set_shot_2(GameObject_t1756533147 * value)
	{
		___shot_2 = value;
		Il2CppCodeGenWriteBarrier((&___shot_2), value);
	}

	inline static int32_t get_offset_of_shotSpawn_3() { return static_cast<int32_t>(offsetof(Done_WeaponController_t1887556875, ___shotSpawn_3)); }
	inline Transform_t3275118058 * get_shotSpawn_3() const { return ___shotSpawn_3; }
	inline Transform_t3275118058 ** get_address_of_shotSpawn_3() { return &___shotSpawn_3; }
	inline void set_shotSpawn_3(Transform_t3275118058 * value)
	{
		___shotSpawn_3 = value;
		Il2CppCodeGenWriteBarrier((&___shotSpawn_3), value);
	}

	inline static int32_t get_offset_of_fireRate_4() { return static_cast<int32_t>(offsetof(Done_WeaponController_t1887556875, ___fireRate_4)); }
	inline float get_fireRate_4() const { return ___fireRate_4; }
	inline float* get_address_of_fireRate_4() { return &___fireRate_4; }
	inline void set_fireRate_4(float value)
	{
		___fireRate_4 = value;
	}

	inline static int32_t get_offset_of_delay_5() { return static_cast<int32_t>(offsetof(Done_WeaponController_t1887556875, ___delay_5)); }
	inline float get_delay_5() const { return ___delay_5; }
	inline float* get_address_of_delay_5() { return &___delay_5; }
	inline void set_delay_5(float value)
	{
		___delay_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONE_WEAPONCONTROLLER_T1887556875_H
#ifndef GAMECONTROLLER_T3607102586_H
#define GAMECONTROLLER_T3607102586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameController
struct  GameController_t3607102586  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject GameController::hazard
	GameObject_t1756533147 * ___hazard_2;
	// UnityEngine.Vector3 GameController::spawnValues
	Vector3_t2243707580  ___spawnValues_3;
	// System.Int32 GameController::hazardCount
	int32_t ___hazardCount_4;
	// System.Single GameController::spawnWait
	float ___spawnWait_5;
	// System.Single GameController::startWait
	float ___startWait_6;
	// System.Single GameController::waveWait
	float ___waveWait_7;
	// UnityEngine.Vector3 GameController::spawnPosition
	Vector3_t2243707580  ___spawnPosition_8;
	// UnityEngine.Quaternion GameController::spawnRotation
	Quaternion_t4030073918  ___spawnRotation_9;
	// System.Int32 GameController::i
	int32_t ___i_10;
	// UnityEngine.GUIText GameController::scoreText
	GUIText_t2411476300 * ___scoreText_11;
	// UnityEngine.GUIText GameController::restartText
	GUIText_t2411476300 * ___restartText_12;
	// UnityEngine.GUIText GameController::gameOverText
	GUIText_t2411476300 * ___gameOverText_13;
	// System.Int32 GameController::score
	int32_t ___score_14;
	// System.Boolean GameController::gameOver
	bool ___gameOver_15;
	// System.Boolean GameController::restart
	bool ___restart_16;

public:
	inline static int32_t get_offset_of_hazard_2() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___hazard_2)); }
	inline GameObject_t1756533147 * get_hazard_2() const { return ___hazard_2; }
	inline GameObject_t1756533147 ** get_address_of_hazard_2() { return &___hazard_2; }
	inline void set_hazard_2(GameObject_t1756533147 * value)
	{
		___hazard_2 = value;
		Il2CppCodeGenWriteBarrier((&___hazard_2), value);
	}

	inline static int32_t get_offset_of_spawnValues_3() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___spawnValues_3)); }
	inline Vector3_t2243707580  get_spawnValues_3() const { return ___spawnValues_3; }
	inline Vector3_t2243707580 * get_address_of_spawnValues_3() { return &___spawnValues_3; }
	inline void set_spawnValues_3(Vector3_t2243707580  value)
	{
		___spawnValues_3 = value;
	}

	inline static int32_t get_offset_of_hazardCount_4() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___hazardCount_4)); }
	inline int32_t get_hazardCount_4() const { return ___hazardCount_4; }
	inline int32_t* get_address_of_hazardCount_4() { return &___hazardCount_4; }
	inline void set_hazardCount_4(int32_t value)
	{
		___hazardCount_4 = value;
	}

	inline static int32_t get_offset_of_spawnWait_5() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___spawnWait_5)); }
	inline float get_spawnWait_5() const { return ___spawnWait_5; }
	inline float* get_address_of_spawnWait_5() { return &___spawnWait_5; }
	inline void set_spawnWait_5(float value)
	{
		___spawnWait_5 = value;
	}

	inline static int32_t get_offset_of_startWait_6() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___startWait_6)); }
	inline float get_startWait_6() const { return ___startWait_6; }
	inline float* get_address_of_startWait_6() { return &___startWait_6; }
	inline void set_startWait_6(float value)
	{
		___startWait_6 = value;
	}

	inline static int32_t get_offset_of_waveWait_7() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___waveWait_7)); }
	inline float get_waveWait_7() const { return ___waveWait_7; }
	inline float* get_address_of_waveWait_7() { return &___waveWait_7; }
	inline void set_waveWait_7(float value)
	{
		___waveWait_7 = value;
	}

	inline static int32_t get_offset_of_spawnPosition_8() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___spawnPosition_8)); }
	inline Vector3_t2243707580  get_spawnPosition_8() const { return ___spawnPosition_8; }
	inline Vector3_t2243707580 * get_address_of_spawnPosition_8() { return &___spawnPosition_8; }
	inline void set_spawnPosition_8(Vector3_t2243707580  value)
	{
		___spawnPosition_8 = value;
	}

	inline static int32_t get_offset_of_spawnRotation_9() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___spawnRotation_9)); }
	inline Quaternion_t4030073918  get_spawnRotation_9() const { return ___spawnRotation_9; }
	inline Quaternion_t4030073918 * get_address_of_spawnRotation_9() { return &___spawnRotation_9; }
	inline void set_spawnRotation_9(Quaternion_t4030073918  value)
	{
		___spawnRotation_9 = value;
	}

	inline static int32_t get_offset_of_i_10() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___i_10)); }
	inline int32_t get_i_10() const { return ___i_10; }
	inline int32_t* get_address_of_i_10() { return &___i_10; }
	inline void set_i_10(int32_t value)
	{
		___i_10 = value;
	}

	inline static int32_t get_offset_of_scoreText_11() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___scoreText_11)); }
	inline GUIText_t2411476300 * get_scoreText_11() const { return ___scoreText_11; }
	inline GUIText_t2411476300 ** get_address_of_scoreText_11() { return &___scoreText_11; }
	inline void set_scoreText_11(GUIText_t2411476300 * value)
	{
		___scoreText_11 = value;
		Il2CppCodeGenWriteBarrier((&___scoreText_11), value);
	}

	inline static int32_t get_offset_of_restartText_12() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___restartText_12)); }
	inline GUIText_t2411476300 * get_restartText_12() const { return ___restartText_12; }
	inline GUIText_t2411476300 ** get_address_of_restartText_12() { return &___restartText_12; }
	inline void set_restartText_12(GUIText_t2411476300 * value)
	{
		___restartText_12 = value;
		Il2CppCodeGenWriteBarrier((&___restartText_12), value);
	}

	inline static int32_t get_offset_of_gameOverText_13() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___gameOverText_13)); }
	inline GUIText_t2411476300 * get_gameOverText_13() const { return ___gameOverText_13; }
	inline GUIText_t2411476300 ** get_address_of_gameOverText_13() { return &___gameOverText_13; }
	inline void set_gameOverText_13(GUIText_t2411476300 * value)
	{
		___gameOverText_13 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverText_13), value);
	}

	inline static int32_t get_offset_of_score_14() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___score_14)); }
	inline int32_t get_score_14() const { return ___score_14; }
	inline int32_t* get_address_of_score_14() { return &___score_14; }
	inline void set_score_14(int32_t value)
	{
		___score_14 = value;
	}

	inline static int32_t get_offset_of_gameOver_15() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___gameOver_15)); }
	inline bool get_gameOver_15() const { return ___gameOver_15; }
	inline bool* get_address_of_gameOver_15() { return &___gameOver_15; }
	inline void set_gameOver_15(bool value)
	{
		___gameOver_15 = value;
	}

	inline static int32_t get_offset_of_restart_16() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___restart_16)); }
	inline bool get_restart_16() const { return ___restart_16; }
	inline bool* get_address_of_restart_16() { return &___restart_16; }
	inline void set_restart_16(bool value)
	{
		___restart_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMECONTROLLER_T3607102586_H
#ifndef DONE_DESTROYBYCONTACT_T866731992_H
#define DONE_DESTROYBYCONTACT_T866731992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Done_DestroyByContact
struct  Done_DestroyByContact_t866731992  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Done_DestroyByContact::explosion
	GameObject_t1756533147 * ___explosion_2;
	// UnityEngine.GameObject Done_DestroyByContact::playerExplosion
	GameObject_t1756533147 * ___playerExplosion_3;
	// System.Int32 Done_DestroyByContact::scoreValue
	int32_t ___scoreValue_4;
	// Done_GameController Done_DestroyByContact::gameController
	Done_GameController_t2490533223 * ___gameController_5;

public:
	inline static int32_t get_offset_of_explosion_2() { return static_cast<int32_t>(offsetof(Done_DestroyByContact_t866731992, ___explosion_2)); }
	inline GameObject_t1756533147 * get_explosion_2() const { return ___explosion_2; }
	inline GameObject_t1756533147 ** get_address_of_explosion_2() { return &___explosion_2; }
	inline void set_explosion_2(GameObject_t1756533147 * value)
	{
		___explosion_2 = value;
		Il2CppCodeGenWriteBarrier((&___explosion_2), value);
	}

	inline static int32_t get_offset_of_playerExplosion_3() { return static_cast<int32_t>(offsetof(Done_DestroyByContact_t866731992, ___playerExplosion_3)); }
	inline GameObject_t1756533147 * get_playerExplosion_3() const { return ___playerExplosion_3; }
	inline GameObject_t1756533147 ** get_address_of_playerExplosion_3() { return &___playerExplosion_3; }
	inline void set_playerExplosion_3(GameObject_t1756533147 * value)
	{
		___playerExplosion_3 = value;
		Il2CppCodeGenWriteBarrier((&___playerExplosion_3), value);
	}

	inline static int32_t get_offset_of_scoreValue_4() { return static_cast<int32_t>(offsetof(Done_DestroyByContact_t866731992, ___scoreValue_4)); }
	inline int32_t get_scoreValue_4() const { return ___scoreValue_4; }
	inline int32_t* get_address_of_scoreValue_4() { return &___scoreValue_4; }
	inline void set_scoreValue_4(int32_t value)
	{
		___scoreValue_4 = value;
	}

	inline static int32_t get_offset_of_gameController_5() { return static_cast<int32_t>(offsetof(Done_DestroyByContact_t866731992, ___gameController_5)); }
	inline Done_GameController_t2490533223 * get_gameController_5() const { return ___gameController_5; }
	inline Done_GameController_t2490533223 ** get_address_of_gameController_5() { return &___gameController_5; }
	inline void set_gameController_5(Done_GameController_t2490533223 * value)
	{
		___gameController_5 = value;
		Il2CppCodeGenWriteBarrier((&___gameController_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONE_DESTROYBYCONTACT_T866731992_H
#ifndef DONE_PLAYERCONTROLLER_T1238573128_H
#define DONE_PLAYERCONTROLLER_T1238573128_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Done_PlayerController
struct  Done_PlayerController_t1238573128  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Done_PlayerController::speed
	float ___speed_2;
	// System.Single Done_PlayerController::tilt
	float ___tilt_3;
	// Done_Boundary Done_PlayerController::boundary
	Done_Boundary_t381809497 * ___boundary_4;
	// UnityEngine.GameObject Done_PlayerController::shot
	GameObject_t1756533147 * ___shot_5;
	// UnityEngine.Transform Done_PlayerController::shotSpawn
	Transform_t3275118058 * ___shotSpawn_6;
	// System.Single Done_PlayerController::fireRate
	float ___fireRate_7;
	// System.Single Done_PlayerController::nextFire
	float ___nextFire_8;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(Done_PlayerController_t1238573128, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_tilt_3() { return static_cast<int32_t>(offsetof(Done_PlayerController_t1238573128, ___tilt_3)); }
	inline float get_tilt_3() const { return ___tilt_3; }
	inline float* get_address_of_tilt_3() { return &___tilt_3; }
	inline void set_tilt_3(float value)
	{
		___tilt_3 = value;
	}

	inline static int32_t get_offset_of_boundary_4() { return static_cast<int32_t>(offsetof(Done_PlayerController_t1238573128, ___boundary_4)); }
	inline Done_Boundary_t381809497 * get_boundary_4() const { return ___boundary_4; }
	inline Done_Boundary_t381809497 ** get_address_of_boundary_4() { return &___boundary_4; }
	inline void set_boundary_4(Done_Boundary_t381809497 * value)
	{
		___boundary_4 = value;
		Il2CppCodeGenWriteBarrier((&___boundary_4), value);
	}

	inline static int32_t get_offset_of_shot_5() { return static_cast<int32_t>(offsetof(Done_PlayerController_t1238573128, ___shot_5)); }
	inline GameObject_t1756533147 * get_shot_5() const { return ___shot_5; }
	inline GameObject_t1756533147 ** get_address_of_shot_5() { return &___shot_5; }
	inline void set_shot_5(GameObject_t1756533147 * value)
	{
		___shot_5 = value;
		Il2CppCodeGenWriteBarrier((&___shot_5), value);
	}

	inline static int32_t get_offset_of_shotSpawn_6() { return static_cast<int32_t>(offsetof(Done_PlayerController_t1238573128, ___shotSpawn_6)); }
	inline Transform_t3275118058 * get_shotSpawn_6() const { return ___shotSpawn_6; }
	inline Transform_t3275118058 ** get_address_of_shotSpawn_6() { return &___shotSpawn_6; }
	inline void set_shotSpawn_6(Transform_t3275118058 * value)
	{
		___shotSpawn_6 = value;
		Il2CppCodeGenWriteBarrier((&___shotSpawn_6), value);
	}

	inline static int32_t get_offset_of_fireRate_7() { return static_cast<int32_t>(offsetof(Done_PlayerController_t1238573128, ___fireRate_7)); }
	inline float get_fireRate_7() const { return ___fireRate_7; }
	inline float* get_address_of_fireRate_7() { return &___fireRate_7; }
	inline void set_fireRate_7(float value)
	{
		___fireRate_7 = value;
	}

	inline static int32_t get_offset_of_nextFire_8() { return static_cast<int32_t>(offsetof(Done_PlayerController_t1238573128, ___nextFire_8)); }
	inline float get_nextFire_8() const { return ___nextFire_8; }
	inline float* get_address_of_nextFire_8() { return &___nextFire_8; }
	inline void set_nextFire_8(float value)
	{
		___nextFire_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONE_PLAYERCONTROLLER_T1238573128_H
#ifndef DONE_RANDOMROTATOR_T4185270575_H
#define DONE_RANDOMROTATOR_T4185270575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Done_RandomRotator
struct  Done_RandomRotator_t4185270575  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Done_RandomRotator::tumble
	float ___tumble_2;

public:
	inline static int32_t get_offset_of_tumble_2() { return static_cast<int32_t>(offsetof(Done_RandomRotator_t4185270575, ___tumble_2)); }
	inline float get_tumble_2() const { return ___tumble_2; }
	inline float* get_address_of_tumble_2() { return &___tumble_2; }
	inline void set_tumble_2(float value)
	{
		___tumble_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONE_RANDOMROTATOR_T4185270575_H
#ifndef DONE_MOVER_T3207174306_H
#define DONE_MOVER_T3207174306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Done_Mover
struct  Done_Mover_t3207174306  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Done_Mover::speed
	float ___speed_2;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(Done_Mover_t3207174306, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONE_MOVER_T3207174306_H
#ifndef DONE_DESTROYBYBOUNDARY_T1508105942_H
#define DONE_DESTROYBYBOUNDARY_T1508105942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Done_DestroyByBoundary
struct  Done_DestroyByBoundary_t1508105942  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONE_DESTROYBYBOUNDARY_T1508105942_H
#ifndef DONE_BGSCROLLER_T1955972008_H
#define DONE_BGSCROLLER_T1955972008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Done_BGScroller
struct  Done_BGScroller_t1955972008  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Done_BGScroller::scrollSpeed
	float ___scrollSpeed_2;
	// System.Single Done_BGScroller::tileSizeZ
	float ___tileSizeZ_3;
	// UnityEngine.Vector3 Done_BGScroller::startPosition
	Vector3_t2243707580  ___startPosition_4;

public:
	inline static int32_t get_offset_of_scrollSpeed_2() { return static_cast<int32_t>(offsetof(Done_BGScroller_t1955972008, ___scrollSpeed_2)); }
	inline float get_scrollSpeed_2() const { return ___scrollSpeed_2; }
	inline float* get_address_of_scrollSpeed_2() { return &___scrollSpeed_2; }
	inline void set_scrollSpeed_2(float value)
	{
		___scrollSpeed_2 = value;
	}

	inline static int32_t get_offset_of_tileSizeZ_3() { return static_cast<int32_t>(offsetof(Done_BGScroller_t1955972008, ___tileSizeZ_3)); }
	inline float get_tileSizeZ_3() const { return ___tileSizeZ_3; }
	inline float* get_address_of_tileSizeZ_3() { return &___tileSizeZ_3; }
	inline void set_tileSizeZ_3(float value)
	{
		___tileSizeZ_3 = value;
	}

	inline static int32_t get_offset_of_startPosition_4() { return static_cast<int32_t>(offsetof(Done_BGScroller_t1955972008, ___startPosition_4)); }
	inline Vector3_t2243707580  get_startPosition_4() const { return ___startPosition_4; }
	inline Vector3_t2243707580 * get_address_of_startPosition_4() { return &___startPosition_4; }
	inline void set_startPosition_4(Vector3_t2243707580  value)
	{
		___startPosition_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONE_BGSCROLLER_T1955972008_H
#ifndef PLAYERCONTROLLER_T4148409433_H
#define PLAYERCONTROLLER_T4148409433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerController
struct  PlayerController_t4148409433  : public MonoBehaviour_t1158329972
{
public:
	// System.Single PlayerController::speed
	float ___speed_2;
	// System.Single PlayerController::tilt
	float ___tilt_3;
	// Boundary PlayerController::boundary
	Boundary_t1794889402 * ___boundary_4;
	// UnityEngine.Rigidbody PlayerController::rb
	Rigidbody_t4233889191 * ___rb_5;
	// UnityEngine.GameObject PlayerController::shot
	GameObject_t1756533147 * ___shot_6;
	// UnityEngine.Transform PlayerController::shotSpawn
	Transform_t3275118058 * ___shotSpawn_7;
	// System.Single PlayerController::fireRate
	float ___fireRate_8;
	// System.Single PlayerController::nextFire
	float ___nextFire_9;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_tilt_3() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___tilt_3)); }
	inline float get_tilt_3() const { return ___tilt_3; }
	inline float* get_address_of_tilt_3() { return &___tilt_3; }
	inline void set_tilt_3(float value)
	{
		___tilt_3 = value;
	}

	inline static int32_t get_offset_of_boundary_4() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___boundary_4)); }
	inline Boundary_t1794889402 * get_boundary_4() const { return ___boundary_4; }
	inline Boundary_t1794889402 ** get_address_of_boundary_4() { return &___boundary_4; }
	inline void set_boundary_4(Boundary_t1794889402 * value)
	{
		___boundary_4 = value;
		Il2CppCodeGenWriteBarrier((&___boundary_4), value);
	}

	inline static int32_t get_offset_of_rb_5() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___rb_5)); }
	inline Rigidbody_t4233889191 * get_rb_5() const { return ___rb_5; }
	inline Rigidbody_t4233889191 ** get_address_of_rb_5() { return &___rb_5; }
	inline void set_rb_5(Rigidbody_t4233889191 * value)
	{
		___rb_5 = value;
		Il2CppCodeGenWriteBarrier((&___rb_5), value);
	}

	inline static int32_t get_offset_of_shot_6() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___shot_6)); }
	inline GameObject_t1756533147 * get_shot_6() const { return ___shot_6; }
	inline GameObject_t1756533147 ** get_address_of_shot_6() { return &___shot_6; }
	inline void set_shot_6(GameObject_t1756533147 * value)
	{
		___shot_6 = value;
		Il2CppCodeGenWriteBarrier((&___shot_6), value);
	}

	inline static int32_t get_offset_of_shotSpawn_7() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___shotSpawn_7)); }
	inline Transform_t3275118058 * get_shotSpawn_7() const { return ___shotSpawn_7; }
	inline Transform_t3275118058 ** get_address_of_shotSpawn_7() { return &___shotSpawn_7; }
	inline void set_shotSpawn_7(Transform_t3275118058 * value)
	{
		___shotSpawn_7 = value;
		Il2CppCodeGenWriteBarrier((&___shotSpawn_7), value);
	}

	inline static int32_t get_offset_of_fireRate_8() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___fireRate_8)); }
	inline float get_fireRate_8() const { return ___fireRate_8; }
	inline float* get_address_of_fireRate_8() { return &___fireRate_8; }
	inline void set_fireRate_8(float value)
	{
		___fireRate_8 = value;
	}

	inline static int32_t get_offset_of_nextFire_9() { return static_cast<int32_t>(offsetof(PlayerController_t4148409433, ___nextFire_9)); }
	inline float get_nextFire_9() const { return ___nextFire_9; }
	inline float* get_address_of_nextFire_9() { return &___nextFire_9; }
	inline void set_nextFire_9(float value)
	{
		___nextFire_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCONTROLLER_T4148409433_H
#ifndef RANDOMROTATOR_T1977862972_H
#define RANDOMROTATOR_T1977862972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RandomRotator
struct  RandomRotator_t1977862972  : public MonoBehaviour_t1158329972
{
public:
	// System.Single RandomRotator::tumble
	float ___tumble_2;
	// UnityEngine.Rigidbody RandomRotator::rb
	Rigidbody_t4233889191 * ___rb_3;

public:
	inline static int32_t get_offset_of_tumble_2() { return static_cast<int32_t>(offsetof(RandomRotator_t1977862972, ___tumble_2)); }
	inline float get_tumble_2() const { return ___tumble_2; }
	inline float* get_address_of_tumble_2() { return &___tumble_2; }
	inline void set_tumble_2(float value)
	{
		___tumble_2 = value;
	}

	inline static int32_t get_offset_of_rb_3() { return static_cast<int32_t>(offsetof(RandomRotator_t1977862972, ___rb_3)); }
	inline Rigidbody_t4233889191 * get_rb_3() const { return ___rb_3; }
	inline Rigidbody_t4233889191 ** get_address_of_rb_3() { return &___rb_3; }
	inline void set_rb_3(Rigidbody_t4233889191 * value)
	{
		___rb_3 = value;
		Il2CppCodeGenWriteBarrier((&___rb_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOMROTATOR_T1977862972_H
#ifndef MOVER_T873752897_H
#define MOVER_T873752897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mover
struct  Mover_t873752897  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Mover::speed
	float ___speed_2;
	// UnityEngine.Rigidbody Mover::rb
	Rigidbody_t4233889191 * ___rb_3;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(Mover_t873752897, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_rb_3() { return static_cast<int32_t>(offsetof(Mover_t873752897, ___rb_3)); }
	inline Rigidbody_t4233889191 * get_rb_3() const { return ___rb_3; }
	inline Rigidbody_t4233889191 ** get_address_of_rb_3() { return &___rb_3; }
	inline void set_rb_3(Rigidbody_t4233889191 * value)
	{
		___rb_3 = value;
		Il2CppCodeGenWriteBarrier((&___rb_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVER_T873752897_H
#ifndef GUITEXT_T2411476300_H
#define GUITEXT_T2411476300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIText
struct  GUIText_t2411476300  : public GUIElement_t3381083099
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUITEXT_T2411476300_H
#ifndef DESTROYBYBOUNDARY_T3535450619_H
#define DESTROYBYBOUNDARY_T3535450619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DestroyByBoundary
struct  DestroyByBoundary_t3535450619  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYBYBOUNDARY_T3535450619_H
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_t1756533147 * m_Items[1];

public:
	inline GameObject_t1756533147 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_t1756533147 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_t1756533147 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GameObject_t1756533147 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_t1756533147 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_t1756533147 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m3347661153_gshared (GameObject_t1756533147 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  RuntimeObject * Object_Instantiate_TisRuntimeObject_m3829784634_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, Vector3_t2243707580  p1, Quaternion_t4030073918  p2, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m4109961936_gshared (Component_t3819376471 * __this, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m3105766835 (Component_t3819376471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m4145850038 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::FindWithTag(System.String)
extern "C"  GameObject_t1756533147 * GameObject_FindWithTag_m1929006324 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m2402264703 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<GameController>()
#define GameObject_GetComponent_TisGameController_t3607102586_m57617221(__this, method) ((  GameController_t3607102586 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m3347661153_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3764089466 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m920475918 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Component::get_tag()
extern "C"  String_t* Component_get_tag_m357168014 (Component_t3819376471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m1790663636 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m2697483695 (Component_t3819376471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2243707580  Transform_get_position_m1104419803 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t4030073918  Transform_get_rotation_m1033555130 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
#define Object_Instantiate_TisGameObject_t1756533147_m3064851704(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (RuntimeObject * /* static, unused */, GameObject_t1756533147 *, Vector3_t2243707580 , Quaternion_t4030073918 , const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m3829784634_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void GameController::GameOver()
extern "C"  void GameController_GameOver_m677971461 (GameController_t3607102586 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::AddScore(System.Int32)
extern "C"  void GameController_AddScore_m1057794813 (GameController_t3607102586 * __this, int32_t ___newScoreValue0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C"  void Object_Destroy_m4279412553 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_time()
extern "C"  float Time_get_time_m2216684562 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Repeat(System.Single,System.Single)
extern "C"  float Mathf_Repeat_m943844734 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C"  Vector3_t2243707580  Vector3_get_forward_m1201659139 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Vector3_op_Multiply_m1351554733 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_op_Addition_m3146764857 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m2469242620 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::FindGameObjectWithTag(System.String)
extern "C"  GameObject_t1756533147 * GameObject_FindGameObjectWithTag_m829057129 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<Done_GameController>()
#define GameObject_GetComponent_TisDone_GameController_t2490533223_m3515554710(__this, method) ((  Done_GameController_t2490533223 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m3347661153_gshared)(__this, method)
// System.Void Done_GameController::GameOver()
extern "C"  void Done_GameController_GameOver_m3639611404 (Done_GameController_t2490533223 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Done_GameController::AddScore(System.Int32)
extern "C"  void Done_GameController_AddScore_m81589966 (Done_GameController_t2490533223 * __this, int32_t ___newScoreValue0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, method) ((  Rigidbody_t4233889191 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m4109961936_gshared)(__this, method)
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_velocity()
extern "C"  Vector3_t2243707580  Rigidbody_get_velocity_m2022666970 (Rigidbody_t4233889191 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Done_EvasiveManeuver::Evade()
extern "C"  RuntimeObject* Done_EvasiveManeuver_Evade_m1091605597 (Done_EvasiveManeuver_t2572524317 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_m2470621050 (MonoBehaviour_t1158329972 * __this, RuntimeObject* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Done_EvasiveManeuver/<Evade>c__Iterator0::.ctor()
extern "C"  void U3CEvadeU3Ec__Iterator0__ctor_m2451732199 (U3CEvadeU3Ec__Iterator0_t1651935502 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m2233168104 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::MoveTowards(System.Single,System.Single,System.Single)
extern "C"  float Mathf_MoveTowards_m1130995897 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m2638739322 (Vector3_t2243707580 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::set_velocity(UnityEngine.Vector3)
extern "C"  void Rigidbody_set_velocity_m2514070071 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_position()
extern "C"  Vector3_t2243707580  Rigidbody_get_position_m3465583110 (Rigidbody_t4233889191 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Clamp_m2354025655 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::set_position(UnityEngine.Vector3)
extern "C"  void Rigidbody_set_position_m47523445 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C"  Quaternion_t4030073918  Quaternion_Euler_m2887458175 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::set_rotation(UnityEngine.Quaternion)
extern "C"  void Rigidbody_set_rotation_m2752020144 (Rigidbody_t4233889191 * __this, Quaternion_t4030073918  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C"  float Random_Range_m2884721203 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C"  void WaitForSeconds__ctor_m1990515539 (WaitForSeconds_t3839502067 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Sign(System.Single)
extern "C"  float Mathf_Sign_m2039143327 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m3232764727 (NotSupportedException_t1793819818 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIText::set_text(System.String)
extern "C"  void GUIText_set_text_m3758377277 (GUIText_t2411476300 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Done_GameController::UpdateScore()
extern "C"  void Done_GameController_UpdateScore_m1617972303 (Done_GameController_t2490533223 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Done_GameController::SpawnWaves()
extern "C"  RuntimeObject* Done_GameController_SpawnWaves_m3768076309 (Done_GameController_t2490533223 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
extern "C"  bool Input_GetKeyDown_m1771960377 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
extern "C"  Scene_t1684909666  SceneManager_GetActiveScene_m2964039490 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SceneManagement.Scene::get_buildIndex()
extern "C"  int32_t Scene_get_buildIndex_m3735680091 (Scene_t1684909666 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32)
extern "C"  void SceneManager_LoadScene_m87258056 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Done_GameController/<SpawnWaves>c__Iterator0::.ctor()
extern "C"  void U3CSpawnWavesU3Ec__Iterator0__ctor_m2496676767 (U3CSpawnWavesU3Ec__Iterator0_t3736144616 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m56707527 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C"  int32_t Random_Range_m694320887 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t4030073918  Quaternion_get_identity_m1561886418 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
extern "C"  Vector3_t2243707580  Transform_get_forward_m1833488937 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetButton(System.String)
extern "C"  bool Input_GetButton_m38251721 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
#define Component_GetComponent_TisAudioSource_t1135106623_m3920278003(__this, method) ((  AudioSource_t1135106623 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m4109961936_gshared)(__this, method)
// System.Void UnityEngine.AudioSource::Play()
extern "C"  void AudioSource_Play_m353744792 (AudioSource_t1135106623 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Input::GetAxis(System.String)
extern "C"  float Input_GetAxis_m2098048324 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Random::get_insideUnitSphere()
extern "C"  Vector3_t2243707580  Random_get_insideUnitSphere_m4012327022 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::set_angularVelocity(UnityEngine.Vector3)
extern "C"  void Rigidbody_set_angularVelocity_m824394045 (Rigidbody_t4233889191 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)
extern "C"  void MonoBehaviour_InvokeRepeating_m3468262484 (MonoBehaviour_t1158329972 * __this, String_t* p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void GameController::UpdateScore()
extern "C"  void GameController_UpdateScore_m1539112084 (GameController_t3607102586 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameController::SpawnWaves()
extern "C"  RuntimeObject* GameController_SpawnWaves_m1643767934 (GameController_t3607102586 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SceneManagement.Scene::get_name()
extern "C"  String_t* Scene_get_name_m745914591 (Scene_t1684909666 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
extern "C"  void SceneManager_LoadScene_m1619949821 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void GameController/<SpawnWaves>c__Iterator0::.ctor()
extern "C"  void U3CSpawnWavesU3Ec__Iterator0__ctor_m1610542038 (U3CSpawnWavesU3Ec__Iterator0_t495518909 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C"  void ScriptableObject__ctor_m2671490429 (ScriptableObject_t1975622470 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Boundary::.ctor()
extern "C"  void Boundary__ctor_m2222466319 (Boundary_t1794889402 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DestroyByBoundary::.ctor()
extern "C"  void DestroyByBoundary__ctor_m3613640316 (DestroyByBoundary_t3535450619 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DestroyByBoundary::OnTriggerExit(UnityEngine.Collider)
extern "C"  void DestroyByBoundary_OnTriggerExit_m4111137786 (DestroyByBoundary_t3535450619 * __this, Collider_t3497673348 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DestroyByBoundary_OnTriggerExit_m4111137786_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider_t3497673348 * L_0 = ___other0;
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DestroyByContact::.ctor()
extern "C"  void DestroyByContact__ctor_m1612351204 (DestroyByContact_t3397533383 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DestroyByContact::Start()
extern "C"  void DestroyByContact_Start_m888149736 (DestroyByContact_t3397533383 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DestroyByContact_Start_m888149736_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral40213808, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1756533147 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		GameObject_t1756533147 * L_3 = V_0;
		GameController_t3607102586 * L_4 = GameObject_GetComponent_TisGameController_t3607102586_m57617221(L_3, /*hidden argument*/GameObject_GetComponent_TisGameController_t3607102586_m57617221_RuntimeMethod_var);
		__this->set_gameController_4(L_4);
	}

IL_0023:
	{
		GameController_t3607102586 * L_5 = __this->get_gameController_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2803466960, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Void DestroyByContact::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void DestroyByContact_OnTriggerEnter_m3552195568 (DestroyByContact_t3397533383 * __this, Collider_t3497673348 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DestroyByContact_OnTriggerEnter_m3552195568_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider_t3497673348 * L_0 = ___other0;
		String_t* L_1 = Component_get_tag_m357168014(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, _stringLiteral2522967920, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		return;
	}

IL_0016:
	{
		GameObject_t1756533147 * L_3 = __this->get_explosion_2();
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_5 = Transform_get_position_m1104419803(L_4, /*hidden argument*/NULL);
		Transform_t3275118058 * L_6 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_7 = Transform_get_rotation_m1033555130(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_3, L_5, L_7, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_RuntimeMethod_var);
		Collider_t3497673348 * L_8 = ___other0;
		String_t* L_9 = Component_get_tag_m357168014(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_9, _stringLiteral1875862075, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_007a;
		}
	}
	{
		GameObject_t1756533147 * L_11 = __this->get_playerExplosion_3();
		Collider_t3497673348 * L_12 = ___other0;
		Transform_t3275118058 * L_13 = Component_get_transform_m2697483695(L_12, /*hidden argument*/NULL);
		Vector3_t2243707580  L_14 = Transform_get_position_m1104419803(L_13, /*hidden argument*/NULL);
		Collider_t3497673348 * L_15 = ___other0;
		Transform_t3275118058 * L_16 = Component_get_transform_m2697483695(L_15, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_17 = Transform_get_rotation_m1033555130(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_11, L_14, L_17, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_RuntimeMethod_var);
		GameController_t3607102586 * L_18 = __this->get_gameController_4();
		GameController_GameOver_m677971461(L_18, /*hidden argument*/NULL);
	}

IL_007a:
	{
		GameController_t3607102586 * L_19 = __this->get_gameController_4();
		int32_t L_20 = __this->get_scoreValue_5();
		GameController_AddScore_m1057794813(L_19, L_20, /*hidden argument*/NULL);
		Collider_t3497673348 * L_21 = ___other0;
		GameObject_t1756533147 * L_22 = Component_get_gameObject_m3105766835(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_23 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DestroyByTime::.ctor()
extern "C"  void DestroyByTime__ctor_m2083793255 (DestroyByTime_t1319923286 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DestroyByTime::Start()
extern "C"  void DestroyByTime_Start_m3534349595 (DestroyByTime_t1319923286 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DestroyByTime_Start_m3534349595_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_lifetime_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4279412553(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Done_BGScroller::.ctor()
extern "C"  void Done_BGScroller__ctor_m101475639 (Done_BGScroller_t1955972008 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Done_BGScroller::Start()
extern "C"  void Done_BGScroller_Start_m2517861123 (Done_BGScroller_t1955972008 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		__this->set_startPosition_4(L_1);
		return;
	}
}
// System.Void Done_BGScroller::Update()
extern "C"  void Done_BGScroller_Update_m3812235626 (Done_BGScroller_t1955972008 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Done_BGScroller_Update_m3812235626_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_scrollSpeed_2();
		float L_2 = __this->get_tileSizeZ_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Repeat_m943844734(NULL /*static, unused*/, ((float)((float)L_0*(float)L_1)), L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_5 = __this->get_startPosition_4();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_6 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_7 = V_0;
		Vector3_t2243707580  L_8 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_5, L_8, /*hidden argument*/NULL);
		Transform_set_position_m2469242620(L_4, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Done_Boundary::.ctor()
extern "C"  void Done_Boundary__ctor_m1746284402 (Done_Boundary_t381809497 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Done_DestroyByBoundary::.ctor()
extern "C"  void Done_DestroyByBoundary__ctor_m1461518857 (Done_DestroyByBoundary_t1508105942 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Done_DestroyByBoundary::OnTriggerExit(UnityEngine.Collider)
extern "C"  void Done_DestroyByBoundary_OnTriggerExit_m1927140173 (Done_DestroyByBoundary_t1508105942 * __this, Collider_t3497673348 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Done_DestroyByBoundary_OnTriggerExit_m1927140173_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider_t3497673348 * L_0 = ___other0;
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Done_DestroyByContact::.ctor()
extern "C"  void Done_DestroyByContact__ctor_m2718458489 (Done_DestroyByContact_t866731992 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Done_DestroyByContact::Start()
extern "C"  void Done_DestroyByContact_Start_m3161031753 (Done_DestroyByContact_t866731992 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Done_DestroyByContact_Start_m3161031753_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = GameObject_FindGameObjectWithTag_m829057129(NULL /*static, unused*/, _stringLiteral40213808, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1756533147 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		GameObject_t1756533147 * L_3 = V_0;
		Done_GameController_t2490533223 * L_4 = GameObject_GetComponent_TisDone_GameController_t2490533223_m3515554710(L_3, /*hidden argument*/GameObject_GetComponent_TisDone_GameController_t2490533223_m3515554710_RuntimeMethod_var);
		__this->set_gameController_5(L_4);
	}

IL_0023:
	{
		Done_GameController_t2490533223 * L_5 = __this->get_gameController_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral250741129, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Void Done_DestroyByContact::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void Done_DestroyByContact_OnTriggerEnter_m1153481469 (Done_DestroyByContact_t866731992 * __this, Collider_t3497673348 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Done_DestroyByContact_OnTriggerEnter_m1153481469_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Collider_t3497673348 * L_0 = ___other0;
		String_t* L_1 = Component_get_tag_m357168014(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, _stringLiteral2522967920, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002a;
		}
	}
	{
		Collider_t3497673348 * L_3 = ___other0;
		String_t* L_4 = Component_get_tag_m357168014(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_4, _stringLiteral1816890106, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002b;
		}
	}

IL_002a:
	{
		return;
	}

IL_002b:
	{
		GameObject_t1756533147 * L_6 = __this->get_explosion_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_005e;
		}
	}
	{
		GameObject_t1756533147 * L_8 = __this->get_explosion_2();
		Transform_t3275118058 * L_9 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_10 = Transform_get_position_m1104419803(L_9, /*hidden argument*/NULL);
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_12 = Transform_get_rotation_m1033555130(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_8, L_10, L_12, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_RuntimeMethod_var);
	}

IL_005e:
	{
		Collider_t3497673348 * L_13 = ___other0;
		String_t* L_14 = Component_get_tag_m357168014(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_14, _stringLiteral1875862075, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00a0;
		}
	}
	{
		GameObject_t1756533147 * L_16 = __this->get_playerExplosion_3();
		Collider_t3497673348 * L_17 = ___other0;
		Transform_t3275118058 * L_18 = Component_get_transform_m2697483695(L_17, /*hidden argument*/NULL);
		Vector3_t2243707580  L_19 = Transform_get_position_m1104419803(L_18, /*hidden argument*/NULL);
		Collider_t3497673348 * L_20 = ___other0;
		Transform_t3275118058 * L_21 = Component_get_transform_m2697483695(L_20, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_22 = Transform_get_rotation_m1033555130(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_16, L_19, L_22, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_RuntimeMethod_var);
		Done_GameController_t2490533223 * L_23 = __this->get_gameController_5();
		Done_GameController_GameOver_m3639611404(L_23, /*hidden argument*/NULL);
	}

IL_00a0:
	{
		Done_GameController_t2490533223 * L_24 = __this->get_gameController_5();
		int32_t L_25 = __this->get_scoreValue_4();
		Done_GameController_AddScore_m81589966(L_24, L_25, /*hidden argument*/NULL);
		Collider_t3497673348 * L_26 = ___other0;
		GameObject_t1756533147 * L_27 = Component_get_gameObject_m3105766835(L_26, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_28 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Done_DestroyByTime::.ctor()
extern "C"  void Done_DestroyByTime__ctor_m4093045664 (Done_DestroyByTime_t1767105437 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Done_DestroyByTime::Start()
extern "C"  void Done_DestroyByTime_Start_m2732963720 (Done_DestroyByTime_t1767105437 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Done_DestroyByTime_Start_m2732963720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_lifetime_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4279412553(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Done_EvasiveManeuver::.ctor()
extern "C"  void Done_EvasiveManeuver__ctor_m618166410 (Done_EvasiveManeuver_t2572524317 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Done_EvasiveManeuver::Start()
extern "C"  void Done_EvasiveManeuver_Start_m589529130 (Done_EvasiveManeuver_t2572524317 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Done_EvasiveManeuver_Start_m589529130_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Rigidbody_t4233889191 * L_0 = Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m520013213_RuntimeMethod_var);
		Vector3_t2243707580  L_1 = Rigidbody_get_velocity_m2022666970(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_z_3();
		__this->set_currentSpeed_9(L_2);
		RuntimeObject* L_3 = Done_EvasiveManeuver_Evade_m1091605597(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Done_EvasiveManeuver::Evade()
extern "C"  RuntimeObject* Done_EvasiveManeuver_Evade_m1091605597 (Done_EvasiveManeuver_t2572524317 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Done_EvasiveManeuver_Evade_m1091605597_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CEvadeU3Ec__Iterator0_t1651935502 * V_0 = NULL;
	{
		U3CEvadeU3Ec__Iterator0_t1651935502 * L_0 = (U3CEvadeU3Ec__Iterator0_t1651935502 *)il2cpp_codegen_object_new(U3CEvadeU3Ec__Iterator0_t1651935502_il2cpp_TypeInfo_var);
		U3CEvadeU3Ec__Iterator0__ctor_m2451732199(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CEvadeU3Ec__Iterator0_t1651935502 * L_1 = V_0;
		L_1->set_U24this_0(__this);
		U3CEvadeU3Ec__Iterator0_t1651935502 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Done_EvasiveManeuver::FixedUpdate()
extern "C"  void Done_EvasiveManeuver_FixedUpdate_m4113228203 (Done_EvasiveManeuver_t2572524317 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Done_EvasiveManeuver_FixedUpdate_m4113228203_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Rigidbody_t4233889191 * L_0 = Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m520013213_RuntimeMethod_var);
		Vector3_t2243707580  L_1 = Rigidbody_get_velocity_m2022666970(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		float L_2 = (&V_1)->get_x_1();
		float L_3 = __this->get_targetManeuver_10();
		float L_4 = __this->get_smoothing_5();
		float L_5 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_6 = Mathf_MoveTowards_m1130995897(NULL /*static, unused*/, L_2, L_3, ((float)((float)L_4*(float)L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		Rigidbody_t4233889191 * L_7 = Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m520013213_RuntimeMethod_var);
		float L_8 = V_0;
		float L_9 = __this->get_currentSpeed_9();
		Vector3_t2243707580  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m2638739322((&L_10), L_8, (0.0f), L_9, /*hidden argument*/NULL);
		Rigidbody_set_velocity_m2514070071(L_7, L_10, /*hidden argument*/NULL);
		Rigidbody_t4233889191 * L_11 = Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m520013213_RuntimeMethod_var);
		Rigidbody_t4233889191 * L_12 = Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m520013213_RuntimeMethod_var);
		Vector3_t2243707580  L_13 = Rigidbody_get_position_m3465583110(L_12, /*hidden argument*/NULL);
		V_2 = L_13;
		float L_14 = (&V_2)->get_x_1();
		Done_Boundary_t381809497 * L_15 = __this->get_boundary_2();
		float L_16 = L_15->get_xMin_0();
		Done_Boundary_t381809497 * L_17 = __this->get_boundary_2();
		float L_18 = L_17->get_xMax_1();
		float L_19 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_14, L_16, L_18, /*hidden argument*/NULL);
		Rigidbody_t4233889191 * L_20 = Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m520013213_RuntimeMethod_var);
		Vector3_t2243707580  L_21 = Rigidbody_get_position_m3465583110(L_20, /*hidden argument*/NULL);
		V_3 = L_21;
		float L_22 = (&V_3)->get_z_3();
		Done_Boundary_t381809497 * L_23 = __this->get_boundary_2();
		float L_24 = L_23->get_zMin_2();
		Done_Boundary_t381809497 * L_25 = __this->get_boundary_2();
		float L_26 = L_25->get_zMax_3();
		float L_27 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_22, L_24, L_26, /*hidden argument*/NULL);
		Vector3_t2243707580  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Vector3__ctor_m2638739322((&L_28), L_19, (0.0f), L_27, /*hidden argument*/NULL);
		Rigidbody_set_position_m47523445(L_11, L_28, /*hidden argument*/NULL);
		Rigidbody_t4233889191 * L_29 = Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m520013213_RuntimeMethod_var);
		Rigidbody_t4233889191 * L_30 = Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m520013213_RuntimeMethod_var);
		Vector3_t2243707580  L_31 = Rigidbody_get_velocity_m2022666970(L_30, /*hidden argument*/NULL);
		V_4 = L_31;
		float L_32 = (&V_4)->get_x_1();
		float L_33 = __this->get_tilt_3();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t4030073918_il2cpp_TypeInfo_var);
		Quaternion_t4030073918  L_34 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), ((float)((float)L_32*(float)((-L_33)))), /*hidden argument*/NULL);
		Rigidbody_set_rotation_m2752020144(L_29, L_34, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Done_EvasiveManeuver/<Evade>c__Iterator0::.ctor()
extern "C"  void U3CEvadeU3Ec__Iterator0__ctor_m2451732199 (U3CEvadeU3Ec__Iterator0_t1651935502 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Done_EvasiveManeuver/<Evade>c__Iterator0::MoveNext()
extern "C"  bool U3CEvadeU3Ec__Iterator0_MoveNext_m3561999753 (U3CEvadeU3Ec__Iterator0_t1651935502 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CEvadeU3Ec__Iterator0_MoveNext_m3561999753_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0029;
			}
			case 1:
			{
				goto IL_006d;
			}
			case 2:
			{
				goto IL_00f0;
			}
			case 3:
			{
				goto IL_0144;
			}
		}
	}
	{
		goto IL_0150;
	}

IL_0029:
	{
		Done_EvasiveManeuver_t2572524317 * L_2 = __this->get_U24this_0();
		Vector2_t2243707579 * L_3 = L_2->get_address_of_startWait_6();
		float L_4 = L_3->get_x_0();
		Done_EvasiveManeuver_t2572524317 * L_5 = __this->get_U24this_0();
		Vector2_t2243707579 * L_6 = L_5->get_address_of_startWait_6();
		float L_7 = L_6->get_y_1();
		float L_8 = Random_Range_m2884721203(NULL /*static, unused*/, L_4, L_7, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_9 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_9, L_8, /*hidden argument*/NULL);
		__this->set_U24current_1(L_9);
		bool L_10 = __this->get_U24disposing_2();
		if (L_10)
		{
			goto IL_0068;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0068:
	{
		goto IL_0152;
	}

IL_006d:
	{
		Done_EvasiveManeuver_t2572524317 * L_11 = __this->get_U24this_0();
		Done_EvasiveManeuver_t2572524317 * L_12 = __this->get_U24this_0();
		float L_13 = L_12->get_dodge_4();
		float L_14 = Random_Range_m2884721203(NULL /*static, unused*/, (1.0f), L_13, /*hidden argument*/NULL);
		Done_EvasiveManeuver_t2572524317 * L_15 = __this->get_U24this_0();
		Transform_t3275118058 * L_16 = Component_get_transform_m2697483695(L_15, /*hidden argument*/NULL);
		Vector3_t2243707580  L_17 = Transform_get_position_m1104419803(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		float L_18 = (&V_1)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_19 = Mathf_Sign_m2039143327(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		L_11->set_targetManeuver_10(((float)((float)L_14*(float)((-L_19)))));
		Done_EvasiveManeuver_t2572524317 * L_20 = __this->get_U24this_0();
		Vector2_t2243707579 * L_21 = L_20->get_address_of_maneuverTime_7();
		float L_22 = L_21->get_x_0();
		Done_EvasiveManeuver_t2572524317 * L_23 = __this->get_U24this_0();
		Vector2_t2243707579 * L_24 = L_23->get_address_of_maneuverTime_7();
		float L_25 = L_24->get_y_1();
		float L_26 = Random_Range_m2884721203(NULL /*static, unused*/, L_22, L_25, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_27 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_27, L_26, /*hidden argument*/NULL);
		__this->set_U24current_1(L_27);
		bool L_28 = __this->get_U24disposing_2();
		if (L_28)
		{
			goto IL_00eb;
		}
	}
	{
		__this->set_U24PC_3(2);
	}

IL_00eb:
	{
		goto IL_0152;
	}

IL_00f0:
	{
		Done_EvasiveManeuver_t2572524317 * L_29 = __this->get_U24this_0();
		L_29->set_targetManeuver_10((0.0f));
		Done_EvasiveManeuver_t2572524317 * L_30 = __this->get_U24this_0();
		Vector2_t2243707579 * L_31 = L_30->get_address_of_maneuverWait_8();
		float L_32 = L_31->get_x_0();
		Done_EvasiveManeuver_t2572524317 * L_33 = __this->get_U24this_0();
		Vector2_t2243707579 * L_34 = L_33->get_address_of_maneuverWait_8();
		float L_35 = L_34->get_y_1();
		float L_36 = Random_Range_m2884721203(NULL /*static, unused*/, L_32, L_35, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_37 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_37, L_36, /*hidden argument*/NULL);
		__this->set_U24current_1(L_37);
		bool L_38 = __this->get_U24disposing_2();
		if (L_38)
		{
			goto IL_013f;
		}
	}
	{
		__this->set_U24PC_3(3);
	}

IL_013f:
	{
		goto IL_0152;
	}

IL_0144:
	{
		goto IL_006d;
	}
	// Dead block : IL_0149: ldarg.0

IL_0150:
	{
		return (bool)0;
	}

IL_0152:
	{
		return (bool)1;
	}
}
// System.Object Done_EvasiveManeuver/<Evade>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CEvadeU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4141720797 (U3CEvadeU3Ec__Iterator0_t1651935502 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object Done_EvasiveManeuver/<Evade>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CEvadeU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1312828709 (U3CEvadeU3Ec__Iterator0_t1651935502 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void Done_EvasiveManeuver/<Evade>c__Iterator0::Dispose()
extern "C"  void U3CEvadeU3Ec__Iterator0_Dispose_m1778244668 (U3CEvadeU3Ec__Iterator0_t1651935502 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void Done_EvasiveManeuver/<Evade>c__Iterator0::Reset()
extern "C"  void U3CEvadeU3Ec__Iterator0_Reset_m4140678854 (U3CEvadeU3Ec__Iterator0_t1651935502 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CEvadeU3Ec__Iterator0_Reset_m4140678854_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Done_GameController::.ctor()
extern "C"  void Done_GameController__ctor_m281151754 (Done_GameController_t2490533223 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Done_GameController::Start()
extern "C"  void Done_GameController_Start_m1927832046 (Done_GameController_t2490533223 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Done_GameController_Start_m1927832046_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_gameOver_11((bool)0);
		__this->set_restart_12((bool)0);
		GUIText_t2411476300 * L_0 = __this->get_restartText_9();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		GUIText_set_text_m3758377277(L_0, L_1, /*hidden argument*/NULL);
		GUIText_t2411476300 * L_2 = __this->get_gameOverText_10();
		String_t* L_3 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		GUIText_set_text_m3758377277(L_2, L_3, /*hidden argument*/NULL);
		__this->set_score_13(0);
		Done_GameController_UpdateScore_m1617972303(__this, /*hidden argument*/NULL);
		RuntimeObject* L_4 = Done_GameController_SpawnWaves_m3768076309(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Done_GameController::Update()
extern "C"  void Done_GameController_Update_m859186471 (Done_GameController_t2490533223 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Done_GameController_Update_m859186471_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Scene_t1684909666  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = __this->get_restart_12();
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyDown_m1771960377(NULL /*static, unused*/, ((int32_t)114), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		Scene_t1684909666  L_2 = SceneManager_GetActiveScene_m2964039490(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = Scene_get_buildIndex_m3735680091((&V_0), /*hidden argument*/NULL);
		SceneManager_LoadScene_m87258056(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Collections.IEnumerator Done_GameController::SpawnWaves()
extern "C"  RuntimeObject* Done_GameController_SpawnWaves_m3768076309 (Done_GameController_t2490533223 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Done_GameController_SpawnWaves_m3768076309_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CSpawnWavesU3Ec__Iterator0_t3736144616 * V_0 = NULL;
	{
		U3CSpawnWavesU3Ec__Iterator0_t3736144616 * L_0 = (U3CSpawnWavesU3Ec__Iterator0_t3736144616 *)il2cpp_codegen_object_new(U3CSpawnWavesU3Ec__Iterator0_t3736144616_il2cpp_TypeInfo_var);
		U3CSpawnWavesU3Ec__Iterator0__ctor_m2496676767(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSpawnWavesU3Ec__Iterator0_t3736144616 * L_1 = V_0;
		L_1->set_U24this_4(__this);
		U3CSpawnWavesU3Ec__Iterator0_t3736144616 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Done_GameController::AddScore(System.Int32)
extern "C"  void Done_GameController_AddScore_m81589966 (Done_GameController_t2490533223 * __this, int32_t ___newScoreValue0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_score_13();
		int32_t L_1 = ___newScoreValue0;
		__this->set_score_13(((int32_t)((int32_t)L_0+(int32_t)L_1)));
		Done_GameController_UpdateScore_m1617972303(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Done_GameController::UpdateScore()
extern "C"  void Done_GameController_UpdateScore_m1617972303 (Done_GameController_t2490533223 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Done_GameController_UpdateScore_m1617972303_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUIText_t2411476300 * L_0 = __this->get_scoreText_8();
		int32_t L_1 = __this->get_score_13();
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1756683522, L_3, /*hidden argument*/NULL);
		GUIText_set_text_m3758377277(L_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Done_GameController::GameOver()
extern "C"  void Done_GameController_GameOver_m3639611404 (Done_GameController_t2490533223 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Done_GameController_GameOver_m3639611404_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUIText_t2411476300 * L_0 = __this->get_gameOverText_10();
		GUIText_set_text_m3758377277(L_0, _stringLiteral2574498909, /*hidden argument*/NULL);
		__this->set_gameOver_11((bool)1);
		return;
	}
}
// System.Void Done_GameController/<SpawnWaves>c__Iterator0::.ctor()
extern "C"  void U3CSpawnWavesU3Ec__Iterator0__ctor_m2496676767 (U3CSpawnWavesU3Ec__Iterator0_t3736144616 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Done_GameController/<SpawnWaves>c__Iterator0::MoveNext()
extern "C"  bool U3CSpawnWavesU3Ec__Iterator0_MoveNext_m1796056073 (U3CSpawnWavesU3Ec__Iterator0_t3736144616 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSpawnWavesU3Ec__Iterator0_MoveNext_m1796056073_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0029;
			}
			case 1:
			{
				goto IL_0053;
			}
			case 2:
			{
				goto IL_0122;
			}
			case 3:
			{
				goto IL_0170;
			}
		}
	}
	{
		goto IL_01b2;
	}

IL_0029:
	{
		Done_GameController_t2490533223 * L_2 = __this->get_U24this_4();
		float L_3 = L_2->get_startWait_6();
		WaitForSeconds_t3839502067 * L_4 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_4, L_3, /*hidden argument*/NULL);
		__this->set_U24current_5(L_4);
		bool L_5 = __this->get_U24disposing_6();
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		__this->set_U24PC_7(1);
	}

IL_004e:
	{
		goto IL_01b4;
	}

IL_0053:
	{
		__this->set_U3CiU3E__1_0(0);
		goto IL_0130;
	}

IL_005f:
	{
		Done_GameController_t2490533223 * L_6 = __this->get_U24this_4();
		GameObjectU5BU5D_t3057952154* L_7 = L_6->get_hazards_2();
		Done_GameController_t2490533223 * L_8 = __this->get_U24this_4();
		GameObjectU5BU5D_t3057952154* L_9 = L_8->get_hazards_2();
		int32_t L_10 = Random_Range_m694320887(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_9)->max_length)))), /*hidden argument*/NULL);
		int32_t L_11 = L_10;
		GameObject_t1756533147 * L_12 = (L_7)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_11));
		__this->set_U3ChazardU3E__2_1(L_12);
		Done_GameController_t2490533223 * L_13 = __this->get_U24this_4();
		Vector3_t2243707580 * L_14 = L_13->get_address_of_spawnValues_3();
		float L_15 = L_14->get_x_1();
		Done_GameController_t2490533223 * L_16 = __this->get_U24this_4();
		Vector3_t2243707580 * L_17 = L_16->get_address_of_spawnValues_3();
		float L_18 = L_17->get_x_1();
		float L_19 = Random_Range_m2884721203(NULL /*static, unused*/, ((-L_15)), L_18, /*hidden argument*/NULL);
		Done_GameController_t2490533223 * L_20 = __this->get_U24this_4();
		Vector3_t2243707580 * L_21 = L_20->get_address_of_spawnValues_3();
		float L_22 = L_21->get_y_2();
		Done_GameController_t2490533223 * L_23 = __this->get_U24this_4();
		Vector3_t2243707580 * L_24 = L_23->get_address_of_spawnValues_3();
		float L_25 = L_24->get_z_3();
		Vector3_t2243707580  L_26;
		memset(&L_26, 0, sizeof(L_26));
		Vector3__ctor_m2638739322((&L_26), L_19, L_22, L_25, /*hidden argument*/NULL);
		__this->set_U3CspawnPositionU3E__2_2(L_26);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t4030073918_il2cpp_TypeInfo_var);
		Quaternion_t4030073918  L_27 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CspawnRotationU3E__2_3(L_27);
		GameObject_t1756533147 * L_28 = __this->get_U3ChazardU3E__2_1();
		Vector3_t2243707580  L_29 = __this->get_U3CspawnPositionU3E__2_2();
		Quaternion_t4030073918  L_30 = __this->get_U3CspawnRotationU3E__2_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_28, L_29, L_30, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_RuntimeMethod_var);
		Done_GameController_t2490533223 * L_31 = __this->get_U24this_4();
		float L_32 = L_31->get_spawnWait_5();
		WaitForSeconds_t3839502067 * L_33 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_33, L_32, /*hidden argument*/NULL);
		__this->set_U24current_5(L_33);
		bool L_34 = __this->get_U24disposing_6();
		if (L_34)
		{
			goto IL_011d;
		}
	}
	{
		__this->set_U24PC_7(2);
	}

IL_011d:
	{
		goto IL_01b4;
	}

IL_0122:
	{
		int32_t L_35 = __this->get_U3CiU3E__1_0();
		__this->set_U3CiU3E__1_0(((int32_t)((int32_t)L_35+(int32_t)1)));
	}

IL_0130:
	{
		int32_t L_36 = __this->get_U3CiU3E__1_0();
		Done_GameController_t2490533223 * L_37 = __this->get_U24this_4();
		int32_t L_38 = L_37->get_hazardCount_4();
		if ((((int32_t)L_36) < ((int32_t)L_38)))
		{
			goto IL_005f;
		}
	}
	{
		Done_GameController_t2490533223 * L_39 = __this->get_U24this_4();
		float L_40 = L_39->get_waveWait_7();
		WaitForSeconds_t3839502067 * L_41 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_41, L_40, /*hidden argument*/NULL);
		__this->set_U24current_5(L_41);
		bool L_42 = __this->get_U24disposing_6();
		if (L_42)
		{
			goto IL_016b;
		}
	}
	{
		__this->set_U24PC_7(3);
	}

IL_016b:
	{
		goto IL_01b4;
	}

IL_0170:
	{
		Done_GameController_t2490533223 * L_43 = __this->get_U24this_4();
		bool L_44 = L_43->get_gameOver_11();
		if (!L_44)
		{
			goto IL_01a6;
		}
	}
	{
		Done_GameController_t2490533223 * L_45 = __this->get_U24this_4();
		GUIText_t2411476300 * L_46 = L_45->get_restartText_9();
		GUIText_set_text_m3758377277(L_46, _stringLiteral2187112517, /*hidden argument*/NULL);
		Done_GameController_t2490533223 * L_47 = __this->get_U24this_4();
		L_47->set_restart_12((bool)1);
		goto IL_01ab;
	}

IL_01a6:
	{
		goto IL_0053;
	}

IL_01ab:
	{
		__this->set_U24PC_7((-1));
	}

IL_01b2:
	{
		return (bool)0;
	}

IL_01b4:
	{
		return (bool)1;
	}
}
// System.Object Done_GameController/<SpawnWaves>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CSpawnWavesU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4073760429 (U3CSpawnWavesU3Ec__Iterator0_t3736144616 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object Done_GameController/<SpawnWaves>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CSpawnWavesU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1613467877 (U3CSpawnWavesU3Ec__Iterator0_t3736144616 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Void Done_GameController/<SpawnWaves>c__Iterator0::Dispose()
extern "C"  void U3CSpawnWavesU3Ec__Iterator0_Dispose_m2699078414 (U3CSpawnWavesU3Ec__Iterator0_t3736144616 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_6((bool)1);
		__this->set_U24PC_7((-1));
		return;
	}
}
// System.Void Done_GameController/<SpawnWaves>c__Iterator0::Reset()
extern "C"  void U3CSpawnWavesU3Ec__Iterator0_Reset_m4222166680 (U3CSpawnWavesU3Ec__Iterator0_t3736144616 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSpawnWavesU3Ec__Iterator0_Reset_m4222166680_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Done_Mover::.ctor()
extern "C"  void Done_Mover__ctor_m3782643515 (Done_Mover_t3207174306 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Done_Mover::Start()
extern "C"  void Done_Mover_Start_m1303039663 (Done_Mover_t3207174306 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Done_Mover_Start_m1303039663_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rigidbody_t4233889191 * L_0 = Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m520013213_RuntimeMethod_var);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = Transform_get_forward_m1833488937(L_1, /*hidden argument*/NULL);
		float L_3 = __this->get_speed_2();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_4 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Rigidbody_set_velocity_m2514070071(L_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Done_PlayerController::.ctor()
extern "C"  void Done_PlayerController__ctor_m2190371255 (Done_PlayerController_t1238573128 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Done_PlayerController::Update()
extern "C"  void Done_PlayerController_Update_m1184947562 (Done_PlayerController_t1238573128 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Done_PlayerController_Update_m1184947562_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetButton_m38251721(NULL /*static, unused*/, _stringLiteral3645101709, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_005e;
		}
	}
	{
		float L_1 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = __this->get_nextFire_8();
		if ((!(((float)L_1) > ((float)L_2))))
		{
			goto IL_005e;
		}
	}
	{
		float L_3 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = __this->get_fireRate_7();
		__this->set_nextFire_8(((float)((float)L_3+(float)L_4)));
		GameObject_t1756533147 * L_5 = __this->get_shot_5();
		Transform_t3275118058 * L_6 = __this->get_shotSpawn_6();
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		Transform_t3275118058 * L_8 = __this->get_shotSpawn_6();
		Quaternion_t4030073918  L_9 = Transform_get_rotation_m1033555130(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_5, L_7, L_9, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_RuntimeMethod_var);
		AudioSource_t1135106623 * L_10 = Component_GetComponent_TisAudioSource_t1135106623_m3920278003(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t1135106623_m3920278003_RuntimeMethod_var);
		AudioSource_Play_m353744792(L_10, /*hidden argument*/NULL);
	}

IL_005e:
	{
		return;
	}
}
// System.Void Done_PlayerController::FixedUpdate()
extern "C"  void Done_PlayerController_FixedUpdate_m2702744912 (Done_PlayerController_t1238573128 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Done_PlayerController_FixedUpdate_m2702744912_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_0 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral855845486, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1635882288, /*hidden argument*/NULL);
		V_1 = L_1;
		float L_2 = V_0;
		float L_3 = V_1;
		Vector3__ctor_m2638739322((&V_2), L_2, (0.0f), L_3, /*hidden argument*/NULL);
		Rigidbody_t4233889191 * L_4 = Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m520013213_RuntimeMethod_var);
		Vector3_t2243707580  L_5 = V_2;
		float L_6 = __this->get_speed_2();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_7 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		Rigidbody_set_velocity_m2514070071(L_4, L_7, /*hidden argument*/NULL);
		Rigidbody_t4233889191 * L_8 = Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m520013213_RuntimeMethod_var);
		Rigidbody_t4233889191 * L_9 = Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m520013213_RuntimeMethod_var);
		Vector3_t2243707580  L_10 = Rigidbody_get_position_m3465583110(L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		float L_11 = (&V_3)->get_x_1();
		Done_Boundary_t381809497 * L_12 = __this->get_boundary_4();
		float L_13 = L_12->get_xMin_0();
		Done_Boundary_t381809497 * L_14 = __this->get_boundary_4();
		float L_15 = L_14->get_xMax_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_16 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_11, L_13, L_15, /*hidden argument*/NULL);
		Rigidbody_t4233889191 * L_17 = Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m520013213_RuntimeMethod_var);
		Vector3_t2243707580  L_18 = Rigidbody_get_position_m3465583110(L_17, /*hidden argument*/NULL);
		V_4 = L_18;
		float L_19 = (&V_4)->get_z_3();
		Done_Boundary_t381809497 * L_20 = __this->get_boundary_4();
		float L_21 = L_20->get_zMin_2();
		Done_Boundary_t381809497 * L_22 = __this->get_boundary_4();
		float L_23 = L_22->get_zMax_3();
		float L_24 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_19, L_21, L_23, /*hidden argument*/NULL);
		Vector3_t2243707580  L_25;
		memset(&L_25, 0, sizeof(L_25));
		Vector3__ctor_m2638739322((&L_25), L_16, (0.0f), L_24, /*hidden argument*/NULL);
		Rigidbody_set_position_m47523445(L_8, L_25, /*hidden argument*/NULL);
		Rigidbody_t4233889191 * L_26 = Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m520013213_RuntimeMethod_var);
		Rigidbody_t4233889191 * L_27 = Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m520013213_RuntimeMethod_var);
		Vector3_t2243707580  L_28 = Rigidbody_get_velocity_m2022666970(L_27, /*hidden argument*/NULL);
		V_5 = L_28;
		float L_29 = (&V_5)->get_x_1();
		float L_30 = __this->get_tilt_3();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t4030073918_il2cpp_TypeInfo_var);
		Quaternion_t4030073918  L_31 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), ((float)((float)L_29*(float)((-L_30)))), /*hidden argument*/NULL);
		Rigidbody_set_rotation_m2752020144(L_26, L_31, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Done_RandomRotator::.ctor()
extern "C"  void Done_RandomRotator__ctor_m3097056026 (Done_RandomRotator_t4185270575 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Done_RandomRotator::Start()
extern "C"  void Done_RandomRotator_Start_m3495564846 (Done_RandomRotator_t4185270575 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Done_RandomRotator_Start_m3495564846_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rigidbody_t4233889191 * L_0 = Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m520013213_RuntimeMethod_var);
		Vector3_t2243707580  L_1 = Random_get_insideUnitSphere_m4012327022(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = __this->get_tumble_2();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_3 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Rigidbody_set_angularVelocity_m824394045(L_0, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Done_WeaponController::.ctor()
extern "C"  void Done_WeaponController__ctor_m4014362028 (Done_WeaponController_t1887556875 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Done_WeaponController::Start()
extern "C"  void Done_WeaponController_Start_m2637236048 (Done_WeaponController_t1887556875 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Done_WeaponController_Start_m2637236048_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->get_delay_5();
		float L_1 = __this->get_fireRate_4();
		MonoBehaviour_InvokeRepeating_m3468262484(__this, _stringLiteral3457519710, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Done_WeaponController::Fire()
extern "C"  void Done_WeaponController_Fire_m86164314 (Done_WeaponController_t1887556875 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Done_WeaponController_Fire_m86164314_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_shot_2();
		Transform_t3275118058 * L_1 = __this->get_shotSpawn_3();
		Vector3_t2243707580  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = __this->get_shotSpawn_3();
		Quaternion_t4030073918  L_4 = Transform_get_rotation_m1033555130(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_0, L_2, L_4, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_RuntimeMethod_var);
		AudioSource_t1135106623 * L_5 = Component_GetComponent_TisAudioSource_t1135106623_m3920278003(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t1135106623_m3920278003_RuntimeMethod_var);
		AudioSource_Play_m353744792(L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::.ctor()
extern "C"  void GameController__ctor_m1439649957 (GameController_t3607102586 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::Start()
extern "C"  void GameController_Start_m239487205 (GameController_t3607102586 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameController_Start_m239487205_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_score_14(0);
		__this->set_gameOver_15((bool)0);
		__this->set_restart_16((bool)0);
		GUIText_t2411476300 * L_0 = __this->get_restartText_12();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		GUIText_set_text_m3758377277(L_0, L_1, /*hidden argument*/NULL);
		GUIText_t2411476300 * L_2 = __this->get_gameOverText_13();
		String_t* L_3 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		GUIText_set_text_m3758377277(L_2, L_3, /*hidden argument*/NULL);
		GameController_UpdateScore_m1539112084(__this, /*hidden argument*/NULL);
		RuntimeObject* L_4 = GameController_SpawnWaves_m1643767934(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::Update()
extern "C"  void GameController_Update_m1556003900 (GameController_t3607102586 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameController_Update_m1556003900_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Scene_t1684909666  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = __this->get_restart_16();
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyDown_m1771960377(NULL /*static, unused*/, ((int32_t)114), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		Scene_t1684909666  L_2 = SceneManager_GetActiveScene_m2964039490(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = Scene_get_name_m745914591((&V_0), /*hidden argument*/NULL);
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Collections.IEnumerator GameController::SpawnWaves()
extern "C"  RuntimeObject* GameController_SpawnWaves_m1643767934 (GameController_t3607102586 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameController_SpawnWaves_m1643767934_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CSpawnWavesU3Ec__Iterator0_t495518909 * V_0 = NULL;
	{
		U3CSpawnWavesU3Ec__Iterator0_t495518909 * L_0 = (U3CSpawnWavesU3Ec__Iterator0_t495518909 *)il2cpp_codegen_object_new(U3CSpawnWavesU3Ec__Iterator0_t495518909_il2cpp_TypeInfo_var);
		U3CSpawnWavesU3Ec__Iterator0__ctor_m1610542038(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSpawnWavesU3Ec__Iterator0_t495518909 * L_1 = V_0;
		L_1->set_U24this_0(__this);
		U3CSpawnWavesU3Ec__Iterator0_t495518909 * L_2 = V_0;
		return L_2;
	}
}
// System.Void GameController::AddScore(System.Int32)
extern "C"  void GameController_AddScore_m1057794813 (GameController_t3607102586 * __this, int32_t ___newScoreValue0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_score_14();
		int32_t L_1 = ___newScoreValue0;
		__this->set_score_14(((int32_t)((int32_t)L_0+(int32_t)L_1)));
		GameController_UpdateScore_m1539112084(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::UpdateScore()
extern "C"  void GameController_UpdateScore_m1539112084 (GameController_t3607102586 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameController_UpdateScore_m1539112084_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUIText_t2411476300 * L_0 = __this->get_scoreText_11();
		int32_t L_1 = __this->get_score_14();
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1756683522, L_3, /*hidden argument*/NULL);
		GUIText_set_text_m3758377277(L_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::GameOver()
extern "C"  void GameController_GameOver_m677971461 (GameController_t3607102586 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameController_GameOver_m677971461_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUIText_t2411476300 * L_0 = __this->get_gameOverText_13();
		GUIText_set_text_m3758377277(L_0, _stringLiteral2574498909, /*hidden argument*/NULL);
		__this->set_gameOver_15((bool)1);
		return;
	}
}
// System.Void GameController/<SpawnWaves>c__Iterator0::.ctor()
extern "C"  void U3CSpawnWavesU3Ec__Iterator0__ctor_m1610542038 (U3CSpawnWavesU3Ec__Iterator0_t495518909 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GameController/<SpawnWaves>c__Iterator0::MoveNext()
extern "C"  bool U3CSpawnWavesU3Ec__Iterator0_MoveNext_m40044830 (U3CSpawnWavesU3Ec__Iterator0_t495518909 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSpawnWavesU3Ec__Iterator0_MoveNext_m40044830_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0029;
			}
			case 1:
			{
				goto IL_0053;
			}
			case 2:
			{
				goto IL_011b;
			}
			case 3:
			{
				goto IL_0173;
			}
		}
	}
	{
		goto IL_01b5;
	}

IL_0029:
	{
		GameController_t3607102586 * L_2 = __this->get_U24this_0();
		float L_3 = L_2->get_startWait_6();
		WaitForSeconds_t3839502067 * L_4 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_4, L_3, /*hidden argument*/NULL);
		__this->set_U24current_1(L_4);
		bool L_5 = __this->get_U24disposing_2();
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_004e:
	{
		goto IL_01b7;
	}

IL_0053:
	{
		GameController_t3607102586 * L_6 = __this->get_U24this_0();
		L_6->set_i_10(0);
		goto IL_012e;
	}

IL_0064:
	{
		GameController_t3607102586 * L_7 = __this->get_U24this_0();
		GameController_t3607102586 * L_8 = __this->get_U24this_0();
		Vector3_t2243707580 * L_9 = L_8->get_address_of_spawnValues_3();
		float L_10 = L_9->get_x_1();
		GameController_t3607102586 * L_11 = __this->get_U24this_0();
		Vector3_t2243707580 * L_12 = L_11->get_address_of_spawnValues_3();
		float L_13 = L_12->get_x_1();
		float L_14 = Random_Range_m2884721203(NULL /*static, unused*/, ((-L_10)), L_13, /*hidden argument*/NULL);
		GameController_t3607102586 * L_15 = __this->get_U24this_0();
		Vector3_t2243707580 * L_16 = L_15->get_address_of_spawnValues_3();
		float L_17 = L_16->get_y_2();
		GameController_t3607102586 * L_18 = __this->get_U24this_0();
		Vector3_t2243707580 * L_19 = L_18->get_address_of_spawnValues_3();
		float L_20 = L_19->get_z_3();
		Vector3_t2243707580  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector3__ctor_m2638739322((&L_21), L_14, L_17, L_20, /*hidden argument*/NULL);
		L_7->set_spawnPosition_8(L_21);
		GameController_t3607102586 * L_22 = __this->get_U24this_0();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t4030073918_il2cpp_TypeInfo_var);
		Quaternion_t4030073918  L_23 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_22->set_spawnRotation_9(L_23);
		GameController_t3607102586 * L_24 = __this->get_U24this_0();
		GameObject_t1756533147 * L_25 = L_24->get_hazard_2();
		GameController_t3607102586 * L_26 = __this->get_U24this_0();
		Vector3_t2243707580  L_27 = L_26->get_spawnPosition_8();
		GameController_t3607102586 * L_28 = __this->get_U24this_0();
		Quaternion_t4030073918  L_29 = L_28->get_spawnRotation_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_25, L_27, L_29, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_RuntimeMethod_var);
		GameController_t3607102586 * L_30 = __this->get_U24this_0();
		float L_31 = L_30->get_spawnWait_5();
		WaitForSeconds_t3839502067 * L_32 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_32, L_31, /*hidden argument*/NULL);
		__this->set_U24current_1(L_32);
		bool L_33 = __this->get_U24disposing_2();
		if (L_33)
		{
			goto IL_0116;
		}
	}
	{
		__this->set_U24PC_3(2);
	}

IL_0116:
	{
		goto IL_01b7;
	}

IL_011b:
	{
		GameController_t3607102586 * L_34 = __this->get_U24this_0();
		GameController_t3607102586 * L_35 = L_34;
		int32_t L_36 = L_35->get_i_10();
		L_35->set_i_10(((int32_t)((int32_t)L_36+(int32_t)1)));
	}

IL_012e:
	{
		GameController_t3607102586 * L_37 = __this->get_U24this_0();
		int32_t L_38 = L_37->get_i_10();
		GameController_t3607102586 * L_39 = __this->get_U24this_0();
		int32_t L_40 = L_39->get_hazardCount_4();
		if ((((int32_t)L_38) < ((int32_t)L_40)))
		{
			goto IL_0064;
		}
	}
	{
		GameController_t3607102586 * L_41 = __this->get_U24this_0();
		float L_42 = L_41->get_waveWait_7();
		WaitForSeconds_t3839502067 * L_43 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_43, L_42, /*hidden argument*/NULL);
		__this->set_U24current_1(L_43);
		bool L_44 = __this->get_U24disposing_2();
		if (L_44)
		{
			goto IL_016e;
		}
	}
	{
		__this->set_U24PC_3(3);
	}

IL_016e:
	{
		goto IL_01b7;
	}

IL_0173:
	{
		GameController_t3607102586 * L_45 = __this->get_U24this_0();
		bool L_46 = L_45->get_gameOver_15();
		if (!L_46)
		{
			goto IL_01a9;
		}
	}
	{
		GameController_t3607102586 * L_47 = __this->get_U24this_0();
		GUIText_t2411476300 * L_48 = L_47->get_restartText_12();
		GUIText_set_text_m3758377277(L_48, _stringLiteral2187112517, /*hidden argument*/NULL);
		GameController_t3607102586 * L_49 = __this->get_U24this_0();
		L_49->set_restart_16((bool)1);
		goto IL_01ae;
	}

IL_01a9:
	{
		goto IL_0053;
	}

IL_01ae:
	{
		__this->set_U24PC_3((-1));
	}

IL_01b5:
	{
		return (bool)0;
	}

IL_01b7:
	{
		return (bool)1;
	}
}
// System.Object GameController/<SpawnWaves>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CSpawnWavesU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2195583544 (U3CSpawnWavesU3Ec__Iterator0_t495518909 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object GameController/<SpawnWaves>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CSpawnWavesU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3773416784 (U3CSpawnWavesU3Ec__Iterator0_t495518909 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void GameController/<SpawnWaves>c__Iterator0::Dispose()
extern "C"  void U3CSpawnWavesU3Ec__Iterator0_Dispose_m1541813199 (U3CSpawnWavesU3Ec__Iterator0_t495518909 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void GameController/<SpawnWaves>c__Iterator0::Reset()
extern "C"  void U3CSpawnWavesU3Ec__Iterator0_Reset_m388647489 (U3CSpawnWavesU3Ec__Iterator0_t495518909 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSpawnWavesU3Ec__Iterator0_Reset_m388647489_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Mover::.ctor()
extern "C"  void Mover__ctor_m2062620528 (Mover_t873752897 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mover::Start()
extern "C"  void Mover_Start_m3361638920 (Mover_t873752897 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Mover_Start_m3361638920_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rigidbody_t4233889191 * L_0 = Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m520013213_RuntimeMethod_var);
		__this->set_rb_3(L_0);
		Rigidbody_t4233889191 * L_1 = __this->get_rb_3();
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = Transform_get_forward_m1833488937(L_2, /*hidden argument*/NULL);
		float L_4 = __this->get_speed_2();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_5 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Rigidbody_set_velocity_m2514070071(L_1, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerController::.ctor()
extern "C"  void PlayerController__ctor_m3280132936 (PlayerController_t4148409433 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerController::Start()
extern "C"  void PlayerController_Start_m3606284888 (PlayerController_t4148409433 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_Start_m3606284888_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rigidbody_t4233889191 * L_0 = Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m520013213_RuntimeMethod_var);
		__this->set_rb_5(L_0);
		return;
	}
}
// System.Void PlayerController::Update()
extern "C"  void PlayerController_Update_m4228472513 (PlayerController_t4148409433 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_Update_m4228472513_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetButton_m38251721(NULL /*static, unused*/, _stringLiteral3645101709, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0053;
		}
	}
	{
		float L_1 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = __this->get_nextFire_9();
		if ((!(((float)L_1) > ((float)L_2))))
		{
			goto IL_0053;
		}
	}
	{
		float L_3 = Time_get_time_m2216684562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = __this->get_fireRate_8();
		__this->set_nextFire_9(((float)((float)L_3+(float)L_4)));
		GameObject_t1756533147 * L_5 = __this->get_shot_6();
		Transform_t3275118058 * L_6 = __this->get_shotSpawn_7();
		Vector3_t2243707580  L_7 = Transform_get_position_m1104419803(L_6, /*hidden argument*/NULL);
		Transform_t3275118058 * L_8 = __this->get_shotSpawn_7();
		Quaternion_t4030073918  L_9 = Transform_get_rotation_m1033555130(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_5, L_7, L_9, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_RuntimeMethod_var);
	}

IL_0053:
	{
		return;
	}
}
// System.Void PlayerController::FixedUpdate()
extern "C"  void PlayerController_FixedUpdate_m1756102567 (PlayerController_t4148409433 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_FixedUpdate_m1756102567_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_0 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral855845486, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = Input_GetAxis_m2098048324(NULL /*static, unused*/, _stringLiteral1635882288, /*hidden argument*/NULL);
		V_1 = L_1;
		float L_2 = V_0;
		float L_3 = V_1;
		Vector3__ctor_m2638739322((&V_2), L_2, (0.0f), L_3, /*hidden argument*/NULL);
		Rigidbody_t4233889191 * L_4 = __this->get_rb_5();
		Vector3_t2243707580  L_5 = V_2;
		float L_6 = __this->get_speed_2();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_7 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		Rigidbody_set_velocity_m2514070071(L_4, L_7, /*hidden argument*/NULL);
		Rigidbody_t4233889191 * L_8 = __this->get_rb_5();
		Rigidbody_t4233889191 * L_9 = __this->get_rb_5();
		Vector3_t2243707580  L_10 = Rigidbody_get_position_m3465583110(L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		float L_11 = (&V_3)->get_x_1();
		Boundary_t1794889402 * L_12 = __this->get_boundary_4();
		float L_13 = L_12->get_xMin_0();
		Boundary_t1794889402 * L_14 = __this->get_boundary_4();
		float L_15 = L_14->get_xMax_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_16 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_11, L_13, L_15, /*hidden argument*/NULL);
		Rigidbody_t4233889191 * L_17 = __this->get_rb_5();
		Vector3_t2243707580  L_18 = Rigidbody_get_position_m3465583110(L_17, /*hidden argument*/NULL);
		V_4 = L_18;
		float L_19 = (&V_4)->get_z_3();
		Boundary_t1794889402 * L_20 = __this->get_boundary_4();
		float L_21 = L_20->get_zMin_2();
		Boundary_t1794889402 * L_22 = __this->get_boundary_4();
		float L_23 = L_22->get_zMax_3();
		float L_24 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_19, L_21, L_23, /*hidden argument*/NULL);
		Vector3_t2243707580  L_25;
		memset(&L_25, 0, sizeof(L_25));
		Vector3__ctor_m2638739322((&L_25), L_16, (0.0f), L_24, /*hidden argument*/NULL);
		Rigidbody_set_position_m47523445(L_8, L_25, /*hidden argument*/NULL);
		Rigidbody_t4233889191 * L_26 = __this->get_rb_5();
		Rigidbody_t4233889191 * L_27 = __this->get_rb_5();
		Vector3_t2243707580  L_28 = Rigidbody_get_velocity_m2022666970(L_27, /*hidden argument*/NULL);
		V_5 = L_28;
		float L_29 = (&V_5)->get_x_1();
		float L_30 = __this->get_tilt_3();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t4030073918_il2cpp_TypeInfo_var);
		Quaternion_t4030073918  L_31 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), ((float)((float)L_29*(float)((-L_30)))), /*hidden argument*/NULL);
		Rigidbody_set_rotation_m2752020144(L_26, L_31, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RandomRotator::.ctor()
extern "C"  void RandomRotator__ctor_m1992091169 (RandomRotator_t1977862972 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RandomRotator::Start()
extern "C"  void RandomRotator_Start_m1184332417 (RandomRotator_t1977862972 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RandomRotator_Start_m1184332417_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rigidbody_t4233889191 * L_0 = Component_GetComponent_TisRigidbody_t4233889191_m520013213(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m520013213_RuntimeMethod_var);
		__this->set_rb_3(L_0);
		Rigidbody_t4233889191 * L_1 = __this->get_rb_3();
		Vector3_t2243707580  L_2 = Random_get_insideUnitSphere_m4012327022(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_3 = __this->get_tumble_2();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_4 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Rigidbody_set_angularVelocity_m824394045(L_1, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Readme::.ctor()
extern "C"  void Readme__ctor_m2239141633 (Readme_t795099402 * __this, const RuntimeMethod* method)
{
	{
		ScriptableObject__ctor_m2671490429(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Readme/Section::.ctor()
extern "C"  void Section__ctor_m2712041435 (Section_t1120322242 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
